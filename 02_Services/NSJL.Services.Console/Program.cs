﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;
using AngleSharp;
using AngleSharp.Extensions;
using AngleSharp.Parser.Html;
using Microsoft.SqlServer.Server;
using System.IO.Compression;
using zlib;
using Microsoft.Win32;
using System.Web.UI.WebControls;
using System.Drawing;
using NSJL.Plugin.Third.Aliyun;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Globalization;
using DJ.ZEF.ThirdParty;
using NSJL.DAL.DataAccess;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace NSJL.Services.Consoles
{
    class Program
    {
        static void Main(string[] args)
        {
            //var db = new SJLDbContext();
            //var list = db.BoxInfo.ToList(); 
            
            #region 测试反射
            //Assembly ass = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + "NSJL.Biz.Background.dll");
            //Type t = ass.GetType("NSJL.Biz.Background.Client.CommBiz");
            ////测试-------
            ////Type t = Type.GetType("NSJL.Biz.Background.Client.CommBiz");   //获取类
            //var met = t.GetMethod("Login");    //获取方法
            //object obj = Activator.CreateInstance(t);  //创建实例
            ////var saddsa=obj.Login("121", "123");
            //Object[] paras = new Object[] { "123", "333" };
            //var cc = met.Invoke(obj, paras);    //调用方法
            #endregion

            //var biz=new InterfaceBiz();
            //biz.GetReplenishment();

            //var ini = new IniFile("Config/Config.ini");
            //var portName = ini.readKey("BoxConfig", "portName");
            //var baudRate = ini.readKey("BoxConfig", "baudRate");
            //var result = LockManager.GetInstance().Start(portName, baudRate.ToInt32());
            //LockManager.GetInstance().SetStart(1);
            //LockManager.GetInstance().SetEnd(12);


            var saddsds = "02-30-30-30-31-38-36-32-31-38-31-0D-8A-E0".Replace("-", "");
            var sadsads = "02-30-30-30-31-38-36-32-31-38-31-43-A1-E0".Replace("-", "");
            var sadsa      = "02-30-30-30-31-38-36-B2-4C-8A-6A-0A-03".Replace("-", "");
            var asdsadsdsa = "02-30-B0-8A-C2-B2-92-31-38-31-0D-0A-03".Replace("-", "");

            var sssss = ConvertHexStringToBytes(sadsa);
            var saddsdsa=Encoding.UTF8.GetString(sssss);


        }


        public static byte[] ConvertHexStringToBytes(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException("参数长度不正确");
            }

            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return returnBytes;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace NSJL.Services.WinServices
{
    class TopshelfService
    {
        public void Start()
        {
            OneMinuteStart();
        }
        public void Stop()
        {
        }
        private void OneMinuteStart()
        {
            try
            {
                var interval = 1000 * 60 * 60 * 2;
                var mt = new Timer(interval);
                mt.Enabled = true;
                mt.Elapsed += (o, e) =>
                {
                    try
                    {
                        var t = (Timer)o;
                        t.AutoReset = false;
                        t.Enabled = false;
                        t.Stop();
                        new StartOwn().Start();
                        t.Enabled = true;
                        t.AutoReset = true;
                        t.Start();
                    }
                    catch (Exception ex)
                    {
                        //TextLogUtil.Error(ex.Message + ex.StackTrace);
                        var t = (Timer)o;
                        t.Enabled = true;
                        t.AutoReset = true;
                        t.Start();
                    }
                };
            }
            catch (Exception ex)
            {
                //TextLogUtil.ErrorWithException("2分钟1次出现问题" + ex.Message, ex);
            }
        }

    }
}

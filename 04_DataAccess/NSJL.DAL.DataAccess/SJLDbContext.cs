﻿using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataAccess
{
    public class SJLDbContext : DbContext
    {
        public SJLDbContext()
            : base(DBConnection.GetConnectionString())
        {
           
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();//移除复数表名的契约

            Database.SetInitializer<SJLDbContext>(null);
        }
        public DbSet<BoxInfo> BoxInfo { get; set; }
        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<AdminInfo> AdminInfo { get; set; }
        public DbSet<AdminLogInfo> AdminLogInfo { get; set; }
        public DbSet<UserLogInfo> UserLogInfo { get; set; }
        public DbSet<VisitorLogInfo> VisitorLogInfo { get; set; }
        public DbSet<ModelInfo> ModelInfo { get; set; }
       
        


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class BoxInfo
    {
        [Key]
        public string Id { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //是否锁定   默认false
        public bool? IsLock { get; set; }
        //是否空闲，1表示是，0是否
        public bool? IsFree { get; set; }
        //最后更新时间
        public DateTime? UpdateTime { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
        //箱门类型，大箱、小箱
        public string Type { get; set; }

    }
}

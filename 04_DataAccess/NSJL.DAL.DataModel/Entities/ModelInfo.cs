﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class ModelInfo
    {
        public string Id {  get; set; }
        //模式名称
        public string ModelName { get; set; }
        //模式类别，0：身份验证方式，1：其他（游客模式、中途取物）
        public string ModelClass { get; set; }
        //是否选择，0：否，1：选择
        public bool IsChoose { get; set; }= false;

    }
}

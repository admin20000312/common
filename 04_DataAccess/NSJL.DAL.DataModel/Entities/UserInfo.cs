﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class UserInfo
    {
        [Key]
        public string Id { get; set; }
        //姓名
        public string UserName { get; set; }
        //账号
        public string Account { get; set; } = null;
        //密码
        public string Password { get; set; } = null;
        //面部Id
        public string FaceId { get; set; } = null;
        //头像 base64
        public string HeadPic { get; set; } = null;
        //卡号
        public string CardNum { get; set; } = null;
        //指纹编码
        public string Finger { get; set; } = null;
        //身份码
        public string  QRCode{ get; set; } = null;
        //指静脉
        public string  FingerVein{ get; set; } = null;
        //虹膜
        public string  Iris{ get; set; } = null;
        //箱门号
        public string BoxNums { get; set; }
        //是否是游客（0:不是、1:是）
        public bool? IsVisitor { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
 
    }
}

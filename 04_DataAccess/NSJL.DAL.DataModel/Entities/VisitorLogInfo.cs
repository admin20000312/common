﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class VisitorLogInfo
    {
        [Key]
        public string Id { get; set; }
        //游客Id(格式："游客名"+序号)
        public string VisitorId { get; set; }
        //箱门编号
        public string BoxNum { get; set; } = null;
        //验证方式
        public string CheckType { get; set; }
        //验证信息
        public string CheckInfo { get; set; }
        //存物时间
        public DateTime? PutTime { get; set; }
        //取物时间
        public DateTime? PickupTime { get; set; }
    }
}

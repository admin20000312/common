﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class UserLogInfo
    {
        [Key]
        public string Id { get; set; }
        //用户名
        public string UserName { get; set; }
        //箱门编号
        public string BoxNum { get; set; } = null;
        //验证方式
        public string CheckType { get; set; }
        //验证信息
        public string CheckInfo { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
       
    }


}

﻿using DJ.Dialog;
using DJ.UserControls;
using NPOI.SS.Formula.Functions;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Path = System.IO.Path;

namespace DJ.Clients.Background
{
    /// <summary>
    /// MainSettingChengeStyleView.xaml 的交互逻辑
    /// </summary>
    public partial class MainSettingChengeStyleView : Window
    {
        public MainSettingChengeStyleView()
        {
            InitializeComponent();
            Refresh();
        }

        public void Refresh()
        {
            string path = Environment.CurrentDirectory + "\\StyleImages";
            var images = Directory.GetFiles(path, ".", SearchOption.TopDirectoryOnly).Where(s => s.EndsWith(".png") || s.EndsWith(".jpg"));
            WrapPanel1.Children.Clear();
            foreach (var i in images)
            {
                StyleImgControl sic = new StyleImgControl();
                sic.Tag = i;
                sic.MouseDown += Sic_MouseDown;
                sic.Margin = new Thickness(50);
                ImageBrush ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(i));
                sic.border1.Background = ib;
                sic.lab1.Content = Path.GetFileNameWithoutExtension(i);
                WrapPanel1.Children.Add(sic);
            }
        }
        


        //切换主题
        private void Sic_MouseDown(object sender, MouseButtonEventArgs e)
        {
            StyleImgControl si = (StyleImgControl)sender;
            var content = si.lab1.Content.ToString();
            if (content == "科技蓝")
            {
                //方式一
                //Collection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
                //Application.Current.Resources.MergedDictionaries[2] = Application.LoadComponent(new Uri(@"Resources\BJ-TechnologyBlue.xaml", UriKind.Relative)) as ResourceDictionary;
                //方式二
                Change("BJ-TechnologyBlue");
                Refresh();
            }
            else if (content == "公安蓝")
            {
                //方式一
                //Collection<ResourceDictionary> mergedDictionaries = Application.Current.Resources.MergedDictionaries;
                //Application.Current.Resources.MergedDictionaries[2] = Application.LoadComponent(new Uri(@"Resources\BJ-Blue.xaml", UriKind.Relative)) as ResourceDictionary;
                Change("BJ-Blue");
                Refresh();
            }
            else
            {
                Change("BJ-TechnologyBlue");
                Refresh();
            }
        }

        //添加主题
        private void Button_AddStyle(object sender, RoutedEventArgs e)
        {
            string fileName = SelfUtil.ChooseFile();
            if (string.IsNullOrWhiteSpace(fileName))
            {
                MessageDialog.ShowDialog("未选择文件");
                return;
            }
            //MessageDialog.ShowDialog("图片路径：" + fileName);
            string lowFileName = Path.GetFileNameWithoutExtension(fileName);
            string destFile = Environment.CurrentDirectory + @"\StyleImages\" + lowFileName + ".png";
            SelfUtil.copyFiles(fileName, destFile);
            Refresh();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        //切换背景主题
        ResourceDictionary langRd = null;
        public void Change(string type)
        {
            if (type != null)
            {
                try
                {
                    //根据名字载入语言文件
                    langRd = Application.LoadComponent(new Uri(@"Resources\" + type + ".xaml", UriKind.Relative)) as ResourceDictionary;
                }
                catch (Exception e2)
                {
                    MessageBox.Show(e2.Message);
                }

                if (langRd != null)
                {
                    Application.Current.Resources.MergedDictionaries[1] = langRd;
                }
            }
        }

    }
}

﻿using DJ.Dialog;
using DJ.UserControls;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// ManagerSettingLocalFaceView.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerSettingLocalFaceView : Window
    {
        CommBiz biz = new CommBiz();
        public ManagerSettingLocalFaceView()
        {
            InitializeComponent();
            Init();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public void Init()
        {
            WrapPanel1.Children.Clear();
            var users = biz.GetAllUserHaveFace();
            foreach (var item in users)
            {
                LocalFaceControl btn = new LocalFaceControl();
                btn.Width = 230;
                btn.Height = 230;
                btn.Tag = item.Id;
                btn.Margin = new Thickness(15);
                btn.Label1.Content = item.UserName;
                btn.Label2.Content = item.CreateTime;
                var base64 = Convert.FromBase64String(item.HeadPic);
                btn.img1.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(base64);
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }

        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (LocalFaceControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                var filepath = "pack://application:,,,/ThemeImage/BoxViewNoChooseBoxBJ01.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            }
            else
            {
                u.IsCheck = true;
                var filepath = "pack://application:,,,/ThemeImage/BoxViewChooseBoxBJ01.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            }
        }

        //删除
        private void btn_Delete(object sender, RoutedEventArgs e)
        {
            List<string> lists = new List<string>();
            var childrens = WrapPanel1.Children;
            foreach (var child in childrens)
            {
                LocalFaceControl lfc = (LocalFaceControl)child;
                if (lfc.IsCheck)
                {
                    lists.Add(lfc.Tag.ToString());
                }
            }
            if (lists.Count <= 0)
            {
                MessageDialog.ShowDialog("请选择人脸");
                return;
            }
            CommonResult res = biz.DeleteUserInfoList(lists);
            MessageDialog.ShowDialog(res.message);
            Init();
        }

        //清空人脸库
        private void btn_DeleteAll(object sender, RoutedEventArgs e)
        {
            btn_SelectAll(sender,null);
            List<string> lists = new List<string>();
            var childrens = WrapPanel1.Children;
            foreach (var child in childrens)
            {
                LocalFaceControl lfc = (LocalFaceControl)child;
                if (lfc.IsCheck)
                {
                    lists.Add(lfc.Tag.ToString());
                }
            }
            CommonResult res = biz.DeleteUserInfoList(lists);
            MessageDialog.ShowDialog(res.message);
            Init();
        }

        //全选
        private void btn_SelectAll(object sender, RoutedEventArgs e)
        {
            Button click = (Button)sender;
            var name = (string)click.Content;
            foreach (LocalFaceControl item in WrapPanel1.Children)
            {
                var u = item;
                u.IsCheck = true;
                var filepath = "pack://application:,,,/ThemeImage/BoxViewChooseBoxBJ01.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            }
        }
    }
}

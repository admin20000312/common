﻿using DJ.Dialog;
using MaterialDesignThemes.Wpf;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// MainSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class MainSettingView : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public MainSettingView()
        {
            InitializeComponent();
            companyName.Content = ini.readKey("MainCompanyShow", "Company");
            phone.Content = ini.readKey("MainCompanyShow", "Phone");
            cabName.Content = ini.readKey("BoxConfig", "cabinetName");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //更换主题
        private void btn_GoNext1(object sender, RoutedEventArgs e)
        {
            MainSettingChengeStyleView mscsv = new MainSettingChengeStyleView();
            mscsv.ShowDialog();
        }

        //公司名称
        private void btn_GoNext2(object sender, RoutedEventArgs e)
        {
            EditMainInfo emi = new EditMainInfo();
            if (emi.ShowDialog() == true)
            {
                string text = emi.text;
                companyName.Content = text;
                ini.writeKey("MainCompanyShow", "Company", text);
            }
        }

        //联系电话
        private void btn_GoNext3(object sender, RoutedEventArgs e)
        {
            EditMainInfo emi = new EditMainInfo();
            if (emi.ShowDialog() == true)
            {
                string text = emi.text;
                phone.Content = text;
                ini.writeKey("MainCompanyShow", "Phone", text);
            }
        }

        //箱柜名称
        private void btn_GoNext4(object sender, RoutedEventArgs e)
        {
            EditMainInfo emi = new EditMainInfo();
            if (emi.ShowDialog() == true)
            {
                string text = emi.text;
                cabName.Content = text;
                ini.writeKey("BoxConfig", "cabinetName", text);
            }
        }

        //公司logo
        private void btn_GoNext5(object sender, RoutedEventArgs e)
        {
            MainSettingLogoView mslv = new MainSettingLogoView();
            mslv.ShowDialog();
        }
    }
}

﻿using DJ.UserControls;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// BoxView.xaml 的交互逻辑
    /// </summary>
    public partial class BoxView : Window
    {
        CommBiz biz = new CommBiz();
        public BoxView()
        {
            InitializeComponent();
            Init();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void Init()
        {
            WrapPanel1.Children.Clear();
            var result = biz.GetBoxListWithUserId();
            var users = biz.GetAllUser();
            var list = result.data.OrderBy(p => p.BoxNum).ToList();
            if (list.Count <= 0)
            {
                return;
            }
            foreach (var item in list)
            {
                BoxControl btn = new BoxControl();
                btn.Width = 230;
                btn.Height = 230;
                btn.Margin = new Thickness(15);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;
                if (item.IsLock == true)
                {
                    btn.Label3.Content = "已锁定";
                    btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label3.Content = "未锁定";
                }
                if (item.IsFree == false)
                {
                    btn.Label20.Text = "存物";
                    btn.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label20.Text = "空闲";
                }
                UserInfo userInfo = users.Where(x => x.BoxNums == item.BoxNum.ToString()).FirstOrDefault();
                if (userInfo != null)
                {
                    btn.Label4.Content = string.IsNullOrWhiteSpace(userInfo.UserName) == true ? null : "绑定：" + userInfo.UserName;
                }
                btn.Label6.Content = item.Type;

                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }

        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                var filepath = "pack://application:,,,/ThemeImage/BoxViewNoChooseBoxBJ01.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            }
            else
            {
                u.IsCheck = true;
                var filepath = "pack://application:,,,/ThemeImage/BoxViewChooseBoxBJ01.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
               
            }
        }



        //全选、全不选
        private void btn_SelectAll(object sender, RoutedEventArgs e)
        {
            Button click = (Button)sender;
            var name = (string)click.Content;
            foreach (BoxControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name != "全选")
                {
                    click.Content = "全选";

                    u.IsCheck = false;

                    var filepath = "pack://application:,,,/ThemeImage/BoxViewNoChooseBoxBJ01.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                }
                else
                {
                    click.Content = "全不选";
                    u.IsCheck = true;

                    var filepath = "pack://application:,,,/ThemeImage/BoxViewChooseBoxBJ01.png";
                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                   
                }
            }
        }
    }
}

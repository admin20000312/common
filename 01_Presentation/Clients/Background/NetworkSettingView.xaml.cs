﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// NetworkSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class NetworkSettingView : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public NetworkSettingView()
        {
            InitializeComponent();
            cb1.IsChecked = ini.readKey("NetworkSet", "IsUsing").ToInt32() == 1 ? true : false;
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void click1(object sender, RoutedEventArgs e)
        {
            if (cb1.IsChecked == true)
            {
                ini.writeKey("NetworkSet", "IsUsing", "1");
            }
            else
            {
                ini.writeKey("NetworkSet", "IsUsing", "0");
            }
        }
    }
}

﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// LogUserView.xaml 的交互逻辑
    /// </summary>
    public partial class LogUserView : Window
    {
        CommBiz biz = new CommBiz();
        public LogUserView()
        {
            InitializeComponent();
            var result = biz.GetAllUserLogInfo();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            dg1.ItemsSource = result.data;
        }
        
        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //清空用户记录
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var result = biz.ClearUserLogInfo();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            MessageDialog.ShowDialog("清空成功");
            dg1.ItemsSource = null;
        }
    }
}

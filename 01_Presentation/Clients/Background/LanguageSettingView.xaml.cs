﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// LanguageSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class LanguageSettingView : Window
    {
        public LanguageSettingView()
        {
            InitializeComponent();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void c_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ToggleButton c = (ToggleButton)sender;

                c1.IsChecked = false;
                c2.IsChecked = false;
                c3.IsChecked = false;
                c.IsChecked = true;
            
        }
    }
}

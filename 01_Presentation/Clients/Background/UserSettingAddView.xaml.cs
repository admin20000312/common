﻿using DJ.Dialog;
using DJ.Third;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// UserSettingAddView.xaml 的交互逻辑
    /// </summary>
    public partial class UserSettingAddView : Window
    {
        UserInfo user = null;
        CommBiz biz = new CommBiz();

        public UserSettingAddView(UserInfo info = null)
        {
            InitializeComponent();
            Task.Run(() =>
            {
                if (info != null)
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        user = info;
                        nameText.Text = info.UserName;
                        accountText.Text = info.Account;
                        pwdText.Text = info.Password;
                        cardNumText.Text = info.CardNum;
                        fingerText.Text = info.Finger;
                        fingerVeinText.Text = info.FingerVein;
                        irisText.Text = info.Iris;
                        faceId = info.FaceId;
                        headPic= info.HeadPic;
                        if (!string.IsNullOrWhiteSpace(info.HeadPic))
                        {
                            byte[] base64 = Convert.FromBase64String(info.HeadPic);
                            faceImg.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(base64);
                        }
                        //查询未绑定的箱门
                        CommonResult<List<int>> res = biz.GetNoBindBoxNum(info.Id);
                        if (res.result)
                        {
                            List<int> data = res.data;
                            Dispatcher.Invoke(new Action(() =>
                            {
                                cb1.Items.Clear();
                                cb1.Items.Add("选择箱门绑定");
                                cb1.SelectedIndex = 0;
                                foreach (int i in data)
                                {
                                    cb1.Items.Add(i);
                                }
                            }));
                        }
                        cb1.Text = string.IsNullOrWhiteSpace(info.BoxNums) ? "选择箱门绑定" : info.BoxNums;
                    }));
                }
                else
                {
                    //查询未绑定的箱门
                    CommonResult<List<int>> res = biz.GetNoBindBoxNum();
                    if (res.result)
                    {
                        List<int> data = res.data;
                        Dispatcher.Invoke(new Action(() =>
                        {
                            cb1.Items.Clear();
                            cb1.Items.Add("选择箱门绑定");
                            cb1.SelectedIndex = 0;
                            foreach (int i in data)
                            {
                                cb1.Items.Add(i);
                            }
                        }));
                    }
                }
            });
        }


        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        //提交
        private void btn_Commit(object sender, RoutedEventArgs e)
        {
            if (user != null) //修改
            {
                if (string.IsNullOrWhiteSpace(nameText.Text))
                {
                    MessageDialog.ShowDialog("用户名不能为空");
                    return;
                }
                if (string.IsNullOrWhiteSpace(accountText.Text) && string.IsNullOrWhiteSpace(pwdText.Text.ToString()))
                {
                    MessageDialog.ShowDialog("账号、密码不能为空");
                    return;
                }
                user.UserName = nameText.Text;
                user.Account = accountText.Text;
                user.Password = pwdText.Text;
                user.CardNum = cardNumText.Text;
                user.Finger = fingerText.Text;
                EditFaceDB();//人脸保存DB库
                if (!b)
                {
                    return;
                }
                user.FaceId = faceId;
                user.HeadPic = headPic;
                user.FingerVein = fingerVeinText.Text;
                user.Iris = irisText.Text;
                string code = nameText.Text + "#" + accountText.Text + "#" + pwdText.Text;
                BitmapImage bitmapImage = QRCode.GetQRCode(code, 200, 200);
                user.QRCode = Convert.ToBase64String(ConvertToBytes(bitmapImage));
                user.BoxNums = cb1.Text == "选择箱门绑定"? null:cb1.Text;
                CommonResult result = biz.EditUserInfo(user);
                if (!result.result)
                {
                    MessageDialog.ShowDialog(result.message);
                    return;
                }
                MessageDialog.ShowDialog("编辑成功");
                this.Close();
            }
            else //新增
            {
                if (string.IsNullOrWhiteSpace(nameText.Text))
                {
                    MessageDialog.ShowDialog("用户名不能为空");
                    return;
                }
                if (string.IsNullOrWhiteSpace(accountText.Text) && string.IsNullOrWhiteSpace(pwdText.Text.ToString()))
                {
                    MessageDialog.ShowDialog("账号、密码不能为空");
                    return;
                }
                UserInfo info = new UserInfo();
                info.UserName = nameText.Text;
                info.Account = accountText.Text;
                info.Password = pwdText.Text;
                info.CardNum = cardNumText.Text;
                info.Finger = fingerText.Text;
                EditFaceDB();//人脸保存DB库
                if (!b)
                {
                    return;
                }
                info.FaceId = faceId;
                info.HeadPic = headPic;
                info.FingerVein = fingerVeinText.Text;
                info.Iris = irisText.Text;
                string code = nameText.Text + "#" + accountText.Text + "#" + pwdText.Text;
                BitmapImage bitmapImage = QRCode.GetQRCode(code, 200, 200);
                info.QRCode = Convert.ToBase64String(ConvertToBytes(bitmapImage));
                info.BoxNums = cb1.Text == "选择箱门绑定" ? null : cb1.Text;
                CommonResult result = biz.EditUserInfo(info);
                MessageDialog.ShowDialog(result.message);
                if (!result.result)
                {
                    MessageDialog.ShowDialog(result.message);
                    return;
                }
                MessageDialog.ShowDialog("添加成功");
                this.Close();
            }
        }


        string faceId = string.Empty;
        string headPic = string.Empty;
        public byte[] faceBytes { get; set; }
        //人脸录入
        private void btnFace_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingFaceSetView msfsv = new ManagerSettingFaceSetView();
            if (msfsv.ShowDialog() == true)
            {
                faceBytes = msfsv.Facebytes;
                faceImg.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(faceBytes);
                headPic = Convert.ToBase64String(faceBytes);
            }
        }



        bool b = true;
        public void EditFaceDB()
        {
            if (faceBytes != null && faceBytes.Length > 0)
            {
                var result = BaiduFaceApiHelper.GetInstance().Contrast(faceBytes);
                if (result.result)
                {
                    if (faceId == result.data)
                    {
                        BaiduFaceApiHelper.GetInstance().UpdateFace(faceId, faceBytes);
                        headPic = Convert.ToBase64String(faceBytes);
                    }
                    else
                    {
                        //去数据库查询人脸ID是否存在 如果存在不让添加  不存在
                        var isf = biz.IsFaceInUserInfo(result.data);
                        if (isf)
                        {
                            MessageDialog.ShowDialog("人脸已存在，无法继续添加");
                            b = false;
                            return;
                        }
                        else
                        {
                            BaiduFaceApiHelper.GetInstance().DeleteFace(result.data);
                            var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                            if (res.result)
                            {
                                faceId = res.data;
                                headPic = Convert.ToBase64String(faceBytes);
                            }
                        }
                    }
                }
                else
                {
                    var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                    if (res.result)
                    {
                        faceId = res.data;
                        headPic = Convert.ToBase64String(faceBytes);
                    }
                }
            }
        }


        //指纹录入
        private void btnFinger_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingFingerSetView msfs = new ManagerSettingFingerSetView();
            if (msfs.ShowDialog() == true)
            {
                fingerText.Text = msfs.FingerString;
            }
        }

        //指静脉录入
        private void btnFingerVein_Click(object sender, RoutedEventArgs e)
        {

        }

        //虹膜
        private void btnIris_Click(object sender, RoutedEventArgs e)
        {

        }





        //Image.Source数据转换成byte[]数据
        private byte[] ConvertToBytes(BitmapSource bitmapSource)
        {
            byte[] buffer = null;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            MemoryStream memoryStream = new MemoryStream();
            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            encoder.Save(memoryStream);
            memoryStream.Position = 0;
            if (memoryStream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(memoryStream))
                {
                    buffer = br.ReadBytes((int)memoryStream.Length);
                }
            }
            memoryStream.Close();
            return buffer;
        }

        public static Bitmap Base64StringToImage(string inputStr)
        {
            try
            {
                byte[] arr = Convert.FromBase64String(inputStr);
                MemoryStream ms = new MemoryStream(arr);
                Bitmap bmp = new Bitmap(ms);
                ms.Close();
                return bmp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return null;
            }
        }
    }
}

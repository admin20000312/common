﻿using DJ.Pages.BackGroundBaseSetPage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// BaseSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class BaseSettingView : Window
    {
        public BaseSettingView()
        {
            InitializeComponent();
            btn_1(btn1, null);
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //锁板串口
        private void btn_1(object sender, RoutedEventArgs e)
        {
            ClearColor();
            frame1.Content = new BasePage1();
            btn1.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
            r1.Visibility = Visibility.Visible;
        }
        //锁板设置
        private void btn_2(object sender, RoutedEventArgs e)
        {
            ClearColor();
            frame1.Content = new BasePage2();
            btn2.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
            r2.Visibility = Visibility.Visible;
        }
        //读卡串口
        private void btn_3(object sender, RoutedEventArgs e)
        {
            ClearColor();
            frame1.Content = new BasePage3();
            btn3.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
            r3.Visibility = Visibility.Visible;
        }
        //箱柜信息
        private void btn_4(object sender, RoutedEventArgs e)
        {
            ClearColor();
            frame1.Content = new BasePage4();
            btn4.Foreground = (Brush)new BrushConverter().ConvertFromString("#FFFFFF");
            r4.Visibility = Visibility.Visible;
        }


        public void ClearColor()
        {
            var children = wp1.Children;
            foreach (var child in children)
            {
                Button btn = (Button)child;
                btn.Foreground = (Brush)new BrushConverter().ConvertFromString("#999999");
                r1.Visibility = Visibility.Hidden;
                r2.Visibility = Visibility.Hidden;
                r3.Visibility = Visibility.Hidden; 
                r4.Visibility = Visibility.Hidden;
            }
        }
    }
}

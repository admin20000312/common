﻿using DJ.Dialog;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// SystemView.xaml 的交互逻辑
    /// </summary>
    public partial class SystemView : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public SystemView()
        {
            InitializeComponent();
            cb1.IsChecked = ini.readKey("VoiceSet", "IsUsing").ToInt32() == 1 ? true:false;
            cb2.IsChecked = ini.readKey("SystemPwd", "IsUsing").ToInt32() == 1 ? true:false;
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        //基础设置
        private void btn_GoNext1(object sender, RoutedEventArgs e)
        {
            BaseSettingView bsv = new BaseSettingView();
            bsv.ShowDialog();
        }

        //首页设置
        private void btn_GoNext2(object sender, RoutedEventArgs e)
        {
            MainSettingView msv = new MainSettingView();
            msv.ShowDialog();
        }

        //网络设置
        private void btn_GoNext3(object sender, RoutedEventArgs e)
        {
            NetworkSettingView nsv = new NetworkSettingView();
            nsv.ShowDialog();
        }

        //语言设置
        private void btn_GoNext4(object sender, RoutedEventArgs e)
        {
            LanguageSettingView lsv = new LanguageSettingView();
            lsv.ShowDialog();
        }

        //使用模式
        private void btn_GoNext5(object sender, RoutedEventArgs e)
        {
            UsingModelSettingView umsv = new UsingModelSettingView();
            umsv.ShowDialog();
        }


        //语言播报
        private void click1(object sender, RoutedEventArgs e)
        {
            if (cb1.IsChecked == true)
            {
                ini.writeKey("VoiceSet", "IsUsing", "1");
            }
            else
            {
                ini.writeKey("VoiceSet", "IsUsing", "0");
            }
        }

        //超管动态密码
        private void click2(object sender, RoutedEventArgs e)
        {
            if(cb2.IsChecked == true)
            {
                ini.writeKey("SystemPwd", "IsUsing","1");
            }
            else
            {
                ini.writeKey("SystemPwd", "IsUsing","0");
            }
        }

        
    }
}

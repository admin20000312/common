﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;

namespace DJ.Clients.Background
{
    /// <summary>
    /// ManagerSettingAddView.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerSettingAddView : Window
    {
        AdminInfo admin = null;
        CommBiz biz = new CommBiz();
        public ManagerSettingAddView(AdminInfo info = null)
        {
            InitializeComponent();
            Task.Run(() =>
            {
                if(info != null)
                {
                    Dispatcher.Invoke(new Action(() =>
                    {
                        admin = info;
                        nameText.Text = info.UserName;
                        accountText.Text = info.Account;
                        pwdText.Text = info.Password;
                        cardNumText.Text = info.CardNum;
                        fingerText.Text = info.Finger;
                        fingerVeinText.Text = info.FingerVein;
                        irisText.Text = info.Iris;
                        faceId = info.FaceId;
                        headPic = info.HeadPic;
                        if (!string.IsNullOrWhiteSpace(info.HeadPic))
                        {
                            byte[] base64 = Convert.FromBase64String(info.HeadPic);
                            faceImg.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(base64);
                        }
                    }));
                }
            });
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }

        //提交
        private void btn_Commit(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(nameText.Text))
            {
                MessageDialog.ShowDialog("用户名不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(accountText.Text) && string.IsNullOrWhiteSpace(pwdText.Text.ToString()))
            {
                MessageDialog.ShowDialog("账号、密码不能为空");
                return;
            }
            if (admin != null) //修改
            {
                admin.UserName = nameText.Text;
                admin.Account = accountText.Text;
                admin.Password = pwdText.Text;
                admin.CardNum = cardNumText.Text;
                admin.Finger = fingerText.Text;
                EditFaceDB();//人脸DB库修改
                if (!b)
                {
                    return;
                }
                admin.FaceId = faceId;
                admin.HeadPic = headPic;
                admin.FingerVein = fingerVeinText.Text;
                admin.Iris = irisText.Text;
                string code = nameText.Text + "#" + accountText.Text + "#" + pwdText.Text;
                BitmapImage bitmapImage = QRCode.GetQRCode(code, 200, 200);
                admin.QRCode = Convert.ToBase64String(ConvertToBytes(bitmapImage));
                CommonResult result = biz.EditAdminInfo(admin);
                if (!result.result)
                {
                    MessageDialog.ShowDialog(result.message);
                    return;
                }
                MessageDialog.ShowDialog("编辑成功");
                this.Close();
            }
            else //新增
            {
                AdminInfo info = new AdminInfo();
                info.UserName = nameText.Text;
                info.Account = accountText.Text;
                info.Password = pwdText.Text;
                info.CardNum = cardNumText.Text;
                info.Finger = fingerText.Text;
                EditFaceDB();//人脸保存DB库
                if (!b)
                {
                    return;
                }
                info.FaceId = faceId;
                info.HeadPic = headPic;
                info.FingerVein = fingerVeinText.Text;
                info.Iris = irisText.Text;
                string code = nameText.Text + "#" + accountText.Text + "#" + pwdText.Text;
                BitmapImage bitmapImage = QRCode.GetQRCode(code,200,200);
                info.QRCode = Convert.ToBase64String(ConvertToBytes(bitmapImage));
                CommonResult result = biz.EditAdminInfo(info);
                if (!result.result)
                {
                    MessageDialog.ShowDialog(result.message);
                    return;
                }
                MessageDialog.ShowDialog("添加成功");
                this.Close();
            }
        }


        string faceId = string.Empty;
        string headPic = string.Empty;
        public byte[] faceBytes { get; set; }
        //人脸录入
        private void btnFace_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingFaceSetView msfsv = new ManagerSettingFaceSetView();
            if (msfsv.ShowDialog() == true)
            {
                faceBytes = msfsv.Facebytes;
                faceImg.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(faceBytes);
                headPic = Convert.ToBase64String(faceBytes);
            }
        }
        bool b = true;
        public void EditFaceDB()
        {
            if (faceBytes != null && faceBytes.Length > 0)
            {
                var result = BaiduFaceApiHelper.GetInstance().Contrast(faceBytes);
                if (result.result)
                {
                    if (faceId == result.data)
                    {
                        BaiduFaceApiHelper.GetInstance().UpdateFace(faceId, faceBytes);
                        headPic = Convert.ToBase64String(faceBytes);
                    }
                    else
                    {
                        //去数据库查询人脸ID是否存在 如果存在不让添加  不存在
                        var isf = biz.IsFaceInAdminInfo(result.data);
                        if (isf)
                        {
                            MessageDialog.ShowDialog("人脸已存在，无法继续添加");
                            b = false;
                            return;
                        }
                        else
                        {
                            BaiduFaceApiHelper.GetInstance().DeleteFace(result.data);
                            var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                            if (res.result)
                            {
                                faceId = res.data;
                                headPic = Convert.ToBase64String(faceBytes);
                            }
                        } 
                    }
                }
                else
                {
                    var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                    if (res.result)
                    {
                        faceId = res.data;
                        headPic = Convert.ToBase64String(faceBytes);
                    }
                }
            }
        }





        //指纹录入
        private void btnFinger_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingFingerSetView msfs = new ManagerSettingFingerSetView();
            if(msfs.ShowDialog() == true)
            {
                fingerText.Text = msfs.FingerString;
            }
        }

        //指静脉录入
        private void btnFingerVein_Click(object sender, RoutedEventArgs e)
        {

        }

        //虹膜
        private void btnIris_Click(object sender, RoutedEventArgs e)
        {

        }





        //Image.Source数据转换成byte[]数据
        private byte[] ConvertToBytes(BitmapSource bitmapSource)
        {
            byte[] buffer = null;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            MemoryStream memoryStream = new MemoryStream();
            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            encoder.Save(memoryStream);
            memoryStream.Position = 0;
            if (memoryStream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(memoryStream))
                {
                    buffer = br.ReadBytes((int)memoryStream.Length);
                }
            }
            memoryStream.Close();
            return buffer;
        }

        public static Bitmap Base64StringToImage(string inputStr)
        {
            try
            {
                byte[] arr = Convert.FromBase64String(inputStr);
                MemoryStream ms = new MemoryStream(arr);
                Bitmap bmp = new Bitmap(ms);
                ms.Close();
                return bmp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                return null;
            }
        }
    }
}

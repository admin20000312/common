﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// LogAdminView.xaml 的交互逻辑
    /// </summary>
    public partial class LogAdminView : Window
    {
        CommBiz biz = new CommBiz();
        public LogAdminView()
        {
            InitializeComponent();
            var result = biz.GetAllAdminLogInfo();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            dg1.ItemsSource = result.data;
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

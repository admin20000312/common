﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// AdminPwdSetView.xaml 的交互逻辑
    /// </summary>
    public partial class AdminPwdSetView : Window
    {
        public AdminPwdSetView()
        {
            InitializeComponent();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //确定
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string pwdOld = oldPwd.Password;
            string pwdNew1 = newPwd.Password;
            string pwdNew2 = okPwd.Password;
            if (string.IsNullOrEmpty(pwdOld))
            {
                MessageBox.Show("老密码不能为空！");
                return;
            }
            if ((string.IsNullOrEmpty(pwdNew1) || string.IsNullOrEmpty(pwdNew2)) && pwdNew1 != pwdNew2)
            {
                MessageBox.Show("输入的两次新密码不一致");
                return;
            }
            string username = Main.UserName;
            CommBiz cb = new CommBiz();
            var result = cb.EditAdminPassword(username, pwdOld, pwdNew2);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            MessageDialog.ShowDialog(result.message);
            oldPwd.Password = null;
            newPwd.Password = null;
            okPwd.Password = null;
            this.DialogResult = true;
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}

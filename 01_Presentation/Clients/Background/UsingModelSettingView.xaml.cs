﻿using NSJL.Biz.Background.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// UsingModelSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class UsingModelSettingView : Window
    {
        CommBiz biz = new CommBiz();
        public UsingModelSettingView()
        {
            InitializeComponent();
            var models = biz.GetModelInfo();
            var childrens = sp1.Children;
            foreach (var child in childrens)
            {
                Grid grid = (Grid)child;
                var gridCs = grid.Children;
                foreach(var c in gridCs)
                {
                    if (c is ToggleButton tb)
                    {
                        tb.IsChecked = models.Where(p => p.ModelName == tb.Tag.ToString()).FirstOrDefault().IsChoose;
                    }
                }
            }
            
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void click(object sender, RoutedEventArgs e)
        {
            ToggleButton tb = (ToggleButton)sender;
            var name = tb.Tag.ToString();
            bool isChecked = (bool)tb.IsChecked;
            var res = biz.EditModelInfoByName(name,isChecked);
            if (!res.result)
            {
                tb.IsChecked = !isChecked;
            }
        }
    }
}

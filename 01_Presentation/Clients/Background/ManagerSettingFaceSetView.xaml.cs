﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// ManagerSettingFaceSetView.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerSettingFaceSetView : Window
    {
        public ManagerSettingFaceSetView()
        {
            InitializeComponent();
            btn_Again(btnAgain,null);
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public ImageSource imgS = null;
        //确认
        private void btn_Ok(object sender, RoutedEventArgs e)
        {
            imgS = img1.Source as ImageSource;
            this.DialogResult = true;
        }

        //重新录入
        private void btn_Again(object sender, RoutedEventArgs e)
        {
            var result = VideoHelper.GetInstance().Start(ImageWith, Success);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                this.Close();
            }
        }

        public void ImageWith(Bitmap map)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    img1.Source = ConvertToBitmapSource(map);

                });
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
        public void Success(byte[] imageBytes)
        {
            try
            {
                Facebytes = (byte[])imageBytes.Clone();
                //Dispatcher.Invoke(() =>
                //{
                //    this.DialogResult = true;
                //    this.Close();
                //});
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
        public byte[] Facebytes = null;
        public static System.Windows.Media.Imaging.BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(btmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }
}

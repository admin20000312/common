﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// LogView.xaml 的交互逻辑
    /// </summary>
    public partial class LogView : Window
    {
        public LogView()
        {
            InitializeComponent();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();   
        }

        //游客使用记录
        private void btn_GoNext1(object sender, RoutedEventArgs e)
        {
            LogVisitorView lvv = new LogVisitorView();
            lvv.ShowDialog();
        }


        //用户使用记录
        private void btn_GoNext2(object sender, RoutedEventArgs e)
        {
            LogUserView luv = new LogUserView();
            luv.ShowDialog();
        }


        //管理员使用记录
        private void btn_GoNext3(object sender, RoutedEventArgs e)
        {
            LogAdminView lav = new LogAdminView();
            lav.ShowDialog();
        }
    }
}

﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// ManagerSettingView.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerSettingView : Window
    {
        CommBiz biz = new CommBiz();
        public ManagerSettingView()
        {
            InitializeComponent();
            Init();
        }
        public void Init()
        {
            var result = biz.GetAllAdminInfo();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            dg1.ItemsSource = result.data;
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //新增管理员
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingAddView msav = new ManagerSettingAddView();
            msav.ShowDialog();
            Init();
        }

        //编辑管理员
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            var user = (AdminInfo)dg1.SelectedItem;
            if (user == null)
            {
                MessageDialog.ShowDialog("未选中管理员");
                return;
            }
            ManagerSettingAddView msav = new ManagerSettingAddView(user);
            msav.ShowDialog();
            Init();
        }

        //删除
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var user = (AdminInfo)dg1.SelectedItem;
            if (user == null)
            {
                MessageDialog.ShowDialog("未选中用户");
                return;
            }
            CommonResult res = biz.DeleteAdminInfo(user.Id);
            MessageDialog.ShowDialog(res.message);
            Init();
        }

        //本地人脸库
        private void btnFaceDB_Click(object sender, RoutedEventArgs e)
        {
            ManagerSettingLocalFaceView mslfv = new ManagerSettingLocalFaceView();
            mslfv.ShowDialog();
        }
    }
}

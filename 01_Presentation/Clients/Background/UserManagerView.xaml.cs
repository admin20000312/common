﻿using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// UserManagerView.xaml 的交互逻辑
    /// </summary>
    public partial class UserManagerView : Window
    {
        CommBiz biz = new CommBiz();
        public UserManagerView()
        {
            InitializeComponent();
            Init();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            UserSettingAddView usav = new UserSettingAddView();
            usav.ShowDialog();
            Init();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            var user = (UserInfo)dg1.SelectedItem;
            if (user == null)
            {
                MessageDialog.ShowDialog("未选中用户");
                return;
            }
            UserSettingAddView usav = new UserSettingAddView(user);
            usav.ShowDialog();
            Init();
        }

        //删除
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var user = (UserInfo)dg1.SelectedItem;
            if (user == null)
            {
                MessageDialog.ShowDialog("未选中用户");
                return;
            }
            CommonResult res = biz.DeleteUserInfo(user.Id);
            MessageDialog.ShowDialog(res.message);
            Init();
        }


        public void Init()
        {
            var result = biz.GetAllUserInfo();
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
                return;
            }
            dg1.ItemsSource = result.data;
        }
    }
}

﻿using DJ.Dialog;
using DJ.UserControls;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace DJ.Clients.Background
{
    /// <summary>
    /// MainSettingLogoView.xaml 的交互逻辑
    /// </summary>
    public partial class MainSettingLogoView : Window
    {
        public MainSettingLogoView()
        {
            InitializeComponent();
            Refresh();
        }

        public void Refresh()
        {
            string path = Environment.CurrentDirectory + "\\LogoImages";
            var images = Directory.GetFiles(path, ".", SearchOption.TopDirectoryOnly).Where(s => s.EndsWith(".png") || s.EndsWith(".jpg"));
            WrapPanel1.Children.Clear();
            foreach (var i in images)
            {
                LogoControl logo = new LogoControl();
                logo.Tag = i;
                logo.MouseDown += Logo_MouseDown;
                logo.Margin = new Thickness(70);
                logo.img1.Source = new BitmapImage(new Uri(i));
                WrapPanel1.Children.Add(logo);
            }
        }

        private void Logo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            LogoControl logo = (LogoControl)sender;
            var path = logo.Tag.ToString();
            var a = Application.Current.Resources.MergedDictionaries;
            foreach (var item in a)
            {
                string name = item.Source.ToString();
                if (name.Contains("BJ-"))
                {
                    var keys = item.Keys;
                    foreach(var key in keys)
                    {
                        if (key.ToString() == "Logo")
                        {
                            item[key] = new BitmapImage(new Uri(path));
                        }
                    }
                }
               
            }
            this.Close();
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //添加
        private void Button_AddStyle(object sender, RoutedEventArgs e)
        {
            string fileName = SelfUtil.ChooseFile();
            if (string.IsNullOrWhiteSpace(fileName))
            {
                MessageDialog.ShowDialog("未选择文件");
                return;
            }
            string lowFileName = Path.GetFileNameWithoutExtension(fileName);
            string destFile = Environment.CurrentDirectory + @"\LogoImages\" + DateTime.Now.ToString("yyyyMMss") + ".png";
            string mes = SelfUtil.copyFiles(fileName, destFile);
            MessageDialog.ShowDialog(mes);
            Refresh();
        }
    }
}

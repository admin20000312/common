﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients.Background
{
    /// <summary>
    /// ManagerSettingFingerSetView.xaml 的交互逻辑
    /// </summary>
    public partial class ManagerSettingFingerSetView : Window
    {
        public ManagerSettingFingerSetView()
        {
            InitializeComponent();
            btn_Again(btnAgain, null);
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_Ok(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        public string FingerString = null;
        private void btn_Again(object sender, RoutedEventArgs e)
        {
            var result = FingerHelper.GetInstance().Start((a, b, c) =>
            {
                FingerString = b;
                Dispatcher.Invoke(() =>
                {
                    img1.Source = ConvertToBitmapSource(a);
                });
            }, null);
        }

        public static BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(btmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }
}

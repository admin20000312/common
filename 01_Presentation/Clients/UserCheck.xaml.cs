﻿using DJ.Dialog;
using DJ.Pages.CheckPage;
using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// UserCheck.xaml 的交互逻辑
    /// </summary>
    public partial class UserCheck : Window
    {
        public UserInfo userInfo;
        public UserCheck()
        {
            InitializeComponent();
            ComWindow.Start(this, lab1);
            
            SJLDbContext db = new SJLDbContext();
            List<ModelInfo> modelInfos = db.ModelInfo.Where(p=>p.ModelClass == "0" && p.IsChoose == true).ToList();
            foreach (ModelInfo modelInfo in modelInfos)
            {
                Button btn = new Button();
                btn.Width = 220;
                btn.Height = 66;
                btn.FontSize = 40;
                btn.Foreground = (Brush)new BrushConverter().ConvertFromString("#D1F5FF");
                btn.Background = null;
                btn.BorderThickness = new Thickness(0);
                btn.Margin = new Thickness(40,9,40,9);
                btn.Style = (System.Windows.Style)this.Resources["ButtonStyle"];
                btn.Content = modelInfo.ModelName;
                btn.Click += Button_Click;
                wp1.Children.Add(btn);
            }
            wp1.Width = modelInfos.Count * 300;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            foreach(Button btn in wp1.Children)
            {
               btn.Background = null;
               btn.Foreground = (Brush)new BrushConverter().ConvertFromString("#D1F5FF ");
            }
            Button button = sender as Button;
            BitmapImage bi = (BitmapImage)FindResource("ClientCheckUserBtnBJ");
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = bi;
            button.Background = ib;
            button.Foreground = (Brush)new BrushConverter().ConvertFromString("#031F4E ");
            var content = button.Content.ToString();
            switch (content)
            {
                case "密码验证":
                    frame1.Content = new PwdPage();
                    break;
                case "人脸验证":
                    frame1.Content = new FacePage();
                    break;
                case "刷卡验证":
                    frame1.Content = new SwipePage();
                    break;
                case "指纹验证":
                    frame1.Content = new FingerPage();
                    break;
                case "指静脉验证":
                    frame1.Content = new FingerVeinPage();
                    break;
                case "虹膜验证":
                    frame1.Content = new IrisPage();
                    break;
                case "扫码验证":
                    frame1.Content = new ScanCodePage();
                    break;
                default:
                    frame1.Content = new PwdPage();
                    break;

            }
        }

        public void Test(UserInfo uf)
        {
            if (uf != null) 
            {
                MessageDialog.ShowDialog("跳转页面");
            }
        }

        //返回
        private void Button_back(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

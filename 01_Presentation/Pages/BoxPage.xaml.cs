﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataModel.Entities;

namespace DJ.Pages
{
    /// <summary>
    /// BoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class BoxPage : Page
    {
        CommBiz biz = new CommBiz();
        public BoxPage()
        {
            InitializeComponent();
            Init();
        }
        public void Init()
        {
            WrapPanel1.Children.Clear();
            var result = biz.GetBoxListWithUserId();
            var users = biz.GetAllUser();
            var list = result.data.OrderBy(p=>p.BoxNum).ToList();
            if (list.Count <= 0)
            {
                return;
            }
            foreach (var item in list)
            {
                BoxControl btn = new BoxControl();
                btn.Width = 154;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;
                if (item.IsLock == true)
                {
                    btn.Label3.Content = "已锁定";
                    btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label3.Content = "未锁定";
                }
                if (item.IsFree == false)
                {
                    btn.Label20.Text = "存物";
                    btn.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label20.Text = "空闲";
                }
                UserInfo userInfo = users.Where(x => x.BoxNums == item.BoxNum.ToString()).FirstOrDefault();
                if (userInfo != null)
                {
                    btn.Label4.Content = string.IsNullOrWhiteSpace(userInfo.UserName) == true ? null : "绑定：" + userInfo.UserName;
                }
                btn.Label6.Content = item.Type;

                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
        }
        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;

                var filepath = "pack://application:,,,/Image/a未选中.png";
                
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                if (u.Label20.Text == "空闲")
                {
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                }
                else
                {
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
             
                u.Label6.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
            }
            else
            {
                u.IsCheck = true;

                var filepath = "pack://application:,,,/Image/a选中.png";

                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                if (u.Label20.Text == "空闲")
                {
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                }
                else
                {
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }

                u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
           
                u.Label6.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            }
        }
        //开箱
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            //if (list.Count > 0)
            //{
            //    string str = string.Join(",", list.ToArray());
            //    Task.Run(() =>
            //    {
            //        foreach (var item in list)
            //        {
            //            LockManager.GetInstance().OpenBox(item);
            //            Thread.Sleep(600);
            //        }
            //    });
            //    var biz=new CommBiz();

            //    var tip = str + "号箱门开箱成功";
            //    VoiceHelper.GetInstance().Start(tip);
            //    biz.EditAdminLogInfo(tip, Main.UserName);
            //    MessageDialog.ShowDialog(str + "开箱成功");
            //}
        }
        //全开
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            //MessageDialog mes=new MessageDialog();
            //mes.TextBlock1.Text = "是否确定操作";
            //if (mes.ShowDialog() != true)
            //{
            //    return;
            //}


            //var biz = new CommBiz();
            //Task.Run(() =>
            //{
            //    var list = biz.GetBoxList();
            //    foreach (var item in list)
            //    {
            //        LockManager.GetInstance().OpenBox(item.BoxNum);
            //        Thread.Sleep(600);
            //    }
            //});

            //var tip = "全开箱门操作成功";
            //VoiceHelper.GetInstance().Start(tip);

            //biz.EditAdminLogInfo(tip, Main.UserName);
            //MessageDialog.ShowDialog("全开箱门操作成功");
        }
        //清箱
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            if (list.Count > 0)
            {
                foreach (BoxControl item in WrapPanel1.Children)
                {
                    if (list.Contains(item.BoxNum))
                    {

                        item.Label20.Text = "";
                        item.Label4.Content = "未借用";
                    }
                }

                //string str = string.Join(",", list.ToArray());
                //biz.ClearBoxList(list);

                //var tip = str + "号箱门清箱成功";
                //VoiceHelper.GetInstance().Start(tip);

                //biz.EditAdminLogInfo(tip, Main.UserName);
                //MessageDialog.ShowDialog(str + "清箱成功");
            }
        }
        //全清
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            MessageDialog mes = new MessageDialog();
            mes.TextBlock1.Text = "是否确定操作";
            if (mes.ShowDialog() != true)
            {
                return;
            }

            foreach (BoxControl item in WrapPanel1.Children)
            {
                item.Label20.Text = "";
                item.Label4.Content = "未借用";
            }

            //var biz = new CommBiz();
            //biz.AllClearBoxinfo();

            //var tip ="全部清箱成功";
            //VoiceHelper.GetInstance().Start(tip);

            //biz.EditAdminLogInfo(tip, Main.UserName);
            //MessageDialog.ShowDialog("全部清箱成功");
        }
        //锁箱
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            //if (list.Count > 0)
            //{
            //    string str = string.Join(",", list.ToArray());
            //    biz.LockBoxList(list);

            //    var tip = str + "号箱门锁箱成功";
            //    VoiceHelper.GetInstance().Start(tip);

            //    biz.EditAdminLogInfo(tip, Main.UserName);
            //    MessageDialog.ShowDialog(str + "锁箱成功");

            //    Init();
            //}
        }
        //解锁
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            //var biz = new CommBiz();
            //var list = GetSelectBox();
            //if (list.Count > 0)
            //{
            //    string str = string.Join(",", list.ToArray());
            //    biz.UnLockBoxList(list);

            //    var tip = str + "号箱门解箱成功";
            //    VoiceHelper.GetInstance().Start(tip);

            //    biz.EditAdminLogInfo(tip, Main.UserName);
            //    MessageDialog.ShowDialog(str + "解箱成功");

            //    Init();
            //}
        }
        private List<int> GetSelectBox()
        {
            List<int> list = new List<int>();
            foreach (BoxControl item in WrapPanel1.Children)
            {
                if (item.IsCheck)
                {
                    list.Add(item.BoxNum);
                }
            }
            return list;
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {                                                                       
            Init();
        }
        //全选
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button click = (Button) sender;
            var name = (string)click.Content;


            foreach (BoxControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name!="全选")
                {
                    click.Content = "全选";
                    
                    u.IsCheck = false;

                    var filepath = "pack://application:,,,/Image/a未选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");

                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    
                    u.Label6.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                }
                else
                {
                    click.Content = "不选";

                    u.IsCheck = true;

                    var filepath = "pack://application:,,,/Image/a选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    
                    u.Label20.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    //u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");

                    u.Label4.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                   
                    u.Label6.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                }
            }
        }
        //修改类型
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var temp = (ComboBoxItem)ComboBox0.SelectedItem;
            var type = (string)temp.Content;
            if (type == "类型")
            {
                MessageDialog.ShowDialog("请选择类型");
                return;
            }

            var biz = new CommBiz();
            var list = GetSelectBox();
            //if (list.Count > 0)
            //{
            //    string str = string.Join(",", list.ToArray());
            //    biz.SetBoxTypeList(list,type);

            //    biz.EditAdminLogInfo(str + "设置成功", Main.UserName);
            //    MessageDialog.ShowDialog(str + "设置成功");

            //    Init();
            //}
        }
        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        //强制归还
        private void Button_Click_32(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();
            //if (list.Count > 0)
            //{
            //    foreach (BoxControl item in WrapPanel1.Children)
            //    {
            //        if (list.Contains(item.BoxNum))
            //        {
            //            item.Label4.Content = "未借用";
            //            item.Label5.Content = "";
            //        }
            //    }
            //    string str = string.Join(",", list.ToArray());
            //    biz.ReturnBoxList(list);

            //    MessageDialog.ShowDialog(str + "归还成功");
            //}
        }
    }
}

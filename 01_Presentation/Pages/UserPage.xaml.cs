﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using NSJL.Biz.Background.Client;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Excel;

namespace DJ.Pages
{
    /// <summary>
    /// UserPage.xaml 的交互逻辑
    /// </summary>
    public partial class UserPage : Page
    {
        public UserPage()
        {
            InitializeComponent();
            Button_Click1(null, null);
        }
        public CommBiz biz=new CommBiz();
        //查询
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = TextBox1.Text;
            //var list=biz.GetUserList(name);
            //DataGrid1.ItemsSource = null;
            //DataGrid1.ItemsSource = list;
        }
        //显示全部
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            //var list = biz.GetUserList(null);
            //DataGrid1.ItemsSource = null;
            //DataGrid1.ItemsSource = list;
        }
        //添加用户
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            //UserEdit user = new UserEdit();
            //var result=user.ShowDialog();
            //if (result != null && result.Value == true)
            //{
            //    var name = user.name.Text;
            //    var depart = user.depart.Text;
            //    var code = user.code.Text;
            //    var fea = user.fingno.Text;
            //    var goodsclasss = user.goodsclasss.Text;
            //    var faceId = user.faceId;
            //    var faceBytes = user.faceBase64;
            //    var pwd = user.pwd.Password;
            //    //显示密码（测试用）
            //    //MessageDialog.ShowDialog(pwd);
            //    var ccresult=biz.EditUserInfo(null, name, depart, code, fea, goodsclasss,faceId,faceBytes,pwd);
            //    MessageDialog.ShowDialog(ccresult.message);
            //    Button_Click1(null, null);
            //}

        }
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            
        }
        //编辑
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            //GetUserInfo info = (GetUserInfo)DataGrid1.SelectedItem;
            //if (info == null)
            //{
            //    MessageDialog.ShowDialog("未选中用户行");
            //    return;
            //}
            //UserEdit user = new UserEdit();
            //user.name.Text = info.Name;
            //user.depart.Text = info.Mobile;
            //user.code.Text = info.Code;
            //user.fingno.Text = info.Features;
            //user.goodsclasss.Text = info.GoodsClasss;
            //user.pwd.Password = info.Password;
            //if(!string.IsNullOrEmpty(info.Headpic))
            //{
            //    //显示数据库中面部编码图像
            //    var base64 = Convert.FromBase64String(info.Headpic);
            //    user.headpic.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(base64);
            //}
            //var faceId = user.faceId;
            //var faceBytes = user.faceBase64;
            //user.ShowDialog();
            //if (user.DialogResult != null && user.DialogResult == true)
            //{
            //    var name = user.name.Text;
            //    var depart = user.depart.Text;
            //    var code = user.code.Text;
            //    var fea = user.fingno.Text;
            //    var goodsclasss = user.goodsclasss.Text;
            //    var pwd = user.pwd.Password;
            //    var ccresult= biz.EditUserInfo(info.Id, name, depart, code, fea, goodsclasss, faceId, faceBytes,pwd);
            //    MessageDialog.ShowDialog(ccresult.message);
            //    if (ccresult.result)
            //    {
            //        info.Name = name;
            //        info.Mobile = depart;
            //        info.Code = code;
            //        info.Features = fea;
            //    }
            //}
        }
        //删除
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            //GetUserInfo info = (GetUserInfo)DataGrid1.SelectedItem;
            //if (info == null)
            //{
            //    MessageDialog.ShowDialog("未选中用户行");
            //    return;
            //}

            //UserDel user = new UserDel();
            //user.name.Content = info.Name;
            //user.deptpart.Content = info.Mobile;
            //user.code.Content = info.Code;
            //user.finger.Content = info.Features;

            //user.ShowDialog();
            //if (user.DialogResult!=null && user.DialogResult==true)
            //{
            //    //需要删除用户
            //    biz.DelUserInfo(info.Id);

            //    Button_Click1(null, null);
            //}
        }


        private void DataGrid1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            //MyClass mys = (MyClass)DataGrid1.SelectedItem;
            //if (mys != null)
            //{
            //    mys.Content = "1564as6sdfj";

            //    //ListData.Remove(mys);
            //    //DataGrid1.ItemsSource = null;
            //    //DataGrid1.ItemsSource = ListData;

            //    var asdsa = mys.Id;
            //    MessageDialog.ShowDialog(mys.Content);
            //}
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Button_Click1(null, null);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //GetUserInfo info = (GetUserInfo)DataGrid1.SelectedItem;
            //if (info == null)
            //{
            //    MessageDialog.ShowDialog("未选中用户行");
            //    return;
            //}
            //var biz=new CommBiz();
            //biz.UnLockUserInfo(info.Id);
            //info.BoxNums = "";
            //MessageDialog.ShowDialog("解绑成功");

        }
        //导出
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //var path = SelfUtil.ChooseSaveFile();
            //if (path == null)
            //{
            //    return;
            //}

            //var biz = new CommBiz();
            //var list = biz.GetUserListWithExcel();
            //var table = SelfUtil.ListToDataTable(list);

            //table.Columns["Id"].ColumnName = "唯一标识";
            //table.Columns["Name"].ColumnName = "姓名";
            //table.Columns["Mobile"].ColumnName = "手机号";
            //table.Columns["Code"].ColumnName = "卡号";
            //table.Columns["GoodsClasss"].ColumnName = "物品分类";
            //table.Columns["Features"].ColumnName = "指纹特征";
            //table.Columns["CreateTime"].ColumnName = "创建时间";
            //table.Columns["Headpic"].ColumnName = "面部编码";
  


            //var excel = new ExcelHelper();
            //excel.FillDataNew("sheet1", table, "", false);
            //excel.SaveExcel(path);

            //MessageDialog.ShowDialog("导出成功");
        }
        //导入
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    string filePath = SelfUtil.ChooseFile();
            //    if (string.IsNullOrWhiteSpace(filePath))
            //    {
            //        return;
            //    }
            //    ExcelHelper excel = new ExcelHelper(filePath);
            //    var dt = excel.GetDataTable("sheet1", true);

            //    var biz = new CommBiz();
            //    biz.ImportExcelWithUserInfo(dt);

            //    MessageDialog.ShowDialog("导入成功");

            //    Button_Click1(null, null);
            //}
            //catch (Exception exception)
            //{
            //    TextLogUtil.Info(exception.Message);
            //    MessageDialog.ShowDialog(exception.Message);
            //}
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}

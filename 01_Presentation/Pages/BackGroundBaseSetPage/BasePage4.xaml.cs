﻿using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.BackGroundBaseSetPage
{
    /// <summary>
    /// BasePage4.xaml 的交互逻辑
    /// </summary>
    public partial class BasePage4 : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public BasePage4()
        {
            InitializeComponent();
            var count = ini.readKey("BoxConfig", "count");
            tb3.Text = count;
        }

        //提交
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string count = tb3.Text;
            ini.writeKey("BoxConfig", "count", count);
        }
    }
}

﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.BackGroundBaseSetPage
{
    /// <summary>
    /// BasePage2.xaml 的交互逻辑
    /// </summary>
    public partial class BasePage2 : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public BasePage2()
        {
            InitializeComponent();
            var startBoxNum = ini.readKey("BoxConfig", "startBoxNum");
            var endBoxNum = ini.readKey("BoxConfig", "endBoxNum");
            tb1.Text = startBoxNum;
            tb2.Text = endBoxNum;
        }

        //提交
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string startBoxNum = tb1.Text;
            string endBoxNum = tb2.Text;
            ini.writeKey("BoxConfig", "startBoxNum", startBoxNum);
            ini.writeKey("BoxConfig", "endBoxNum", endBoxNum);
            LockManager.GetInstance().Close();
            LockManager.GetInstance().SetStart(startBoxNum.ToInt32());
            LockManager.GetInstance().SetEnd(endBoxNum.ToInt32());
            MessageDialog.ShowDialog("操作成功");
        }
    }
}

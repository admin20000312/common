﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.BackGroundBaseSetPage
{
    /// <summary>
    /// 刷卡器串口设置
    /// </summary>
    public partial class BasePage3 : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public BasePage3()
        {
            InitializeComponent();
            var portName = ini.readKey("CardConfig", "portName");
            var baudRate = ini.readKey("CardConfig", "baudRate");
            cb1.Text = portName;
            cb2.Text = baudRate;
        }


        //提交
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string port = cb1.Text;
            string baud = cb2.Text;
            ini.writeKey("CardConfig", "portName", port);
            ini.writeKey("CardConfig", "baudRate", baud);
            MessageDialog.ShowDialog("操作成功");
        }
    }
}

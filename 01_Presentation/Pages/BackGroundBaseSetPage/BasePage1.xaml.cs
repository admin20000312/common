﻿using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.BackGroundBaseSetPage
{
    /// <summary>
    /// BasePage1.xaml 的交互逻辑
    /// </summary>
    public partial class BasePage1 : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public BasePage1()
        {
            InitializeComponent();
            var portName = ini.readKey("BoxConfig", "portName");
            var baudRate = ini.readKey("BoxConfig", "baudRate");
            cb1.Text = portName;
            cb2.Text = baudRate;
        }

        //提交
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string port = cb1.Text;
            string baud = cb2.Text;
            ini.writeKey("BoxConfig", "portName",port);
            ini.writeKey("BoxConfig", "baudRate",baud);
            LockManager.GetInstance().Close();
            LockManager.GetInstance().Start(port, baud.ToInt32());
            MessageDialog.ShowDialog("操作成功");
        }
    }
}

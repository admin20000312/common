﻿using DJ.Clients;
using DJ.Dialog;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.CheckPage
{
    /// <summary>
    /// FacePage.xaml 的交互逻辑
    /// </summary>
    public partial class FacePage : Page
    {
        CommBiz biz = new CommBiz();
        public FacePage()
        {
            InitializeComponent();
            var result = VideoHelper.GetInstance().Start(ShowImage, FaceResult);
            if (!result.result)
            {
                MessageDialog.ShowDialog(result.message);
            }
        }


        public byte[] Facebytes = null;
        private void Button_OK(object sender, RoutedEventArgs e)
        {

        }


        public void ShowImage(Bitmap a)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    Image1.Source = ConvertToBitmapSource(a);
                });
            }
            catch (Exception e)
            {
            }

        }
        public void FaceResult(byte[] b)
        {
            var call = BaiduFaceApiHelper.GetInstance().Contrast(b);
                try
                {
                    var FaceId = call.data;
                    //根据面部id查询用户是否存在
                    var result = biz.GetUserInfoWithFaceId(FaceId);
                    if (result.result) //用户存在,拿到用户信息
                    {
                        var userInfo = result.data;
                        UserCheck uc = new UserCheck();
                        uc.Test(userInfo);
                    }
                    else //用户不存在
                    {
                        VoiceHelper.GetInstance().Start(result.message);
                        MessageDialog.ShowDialog(result.message);
                    }
                }
                catch (Exception e)
                {
                    TextLogUtil.Info("面部识别中异常：" + e.Message);
                    MessageDialog.ShowDialog("面部识别中异常：" + e.Message);
                    
                }
        }




        public static System.Windows.Media.Imaging.BitmapSource ConvertToBitmapSource(Bitmap btmap)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(btmap.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }
    }
}

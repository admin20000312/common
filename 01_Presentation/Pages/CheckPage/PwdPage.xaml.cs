﻿using DJ.Clients;
using DJ.Dialog;
using NPOI.SS.Formula.Functions;
using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages.CheckPage
{
    /// <summary>
    /// PwdPage.xaml 的交互逻辑
    /// </summary>
    public partial class PwdPage : Page
    {
        public PwdPage()
        {
            InitializeComponent();
        }

        //确定
        private void Button_OK(object sender, RoutedEventArgs e)
        {
            string acc = account.Text;
            string pwd = password.Text;
            SJLDbContext db = new SJLDbContext();
            var userInfo = db.UserInfo.Where(p=>p.Account == acc && p.Password == pwd).FirstOrDefault();
            if (userInfo == null)
            {
                MessageDialog.ShowDialog("账号或密码不正确");
                return;
            }
            UserCheck uc = new UserCheck();
            uc.userInfo = userInfo;
            MessageDialog.ShowDialog("验证成功");
            uc.Test(userInfo);
        }
    }
}

﻿using DJ.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Dialog
{
    /// <summary>
    /// EditMainInfo.xaml 的交互逻辑
    /// </summary>
    public partial class EditMainInfo : Window
    {
        public EditMainInfo()
        {
            InitializeComponent();
        }

        public string text = string.Empty;
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            text = tb1.Text;
            DialogResult = true;
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}

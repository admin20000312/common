﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Third;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using DJ.Clients;

namespace DJ.Dialog
{
    /// <summary>
    /// UserEdit.xaml 的交互逻辑
    /// </summary>
    public partial class UserEdit : Window
    {
        public string faceId { get; set; }
        public string faceBase64 { get; set; }
        public string UserId { get; set; }
        public byte[] faceBytes { get; set; }
        public UserEdit()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(name.Text))
            {
                MessageDialog.ShowDialog("用户名不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(code.Text) && string.IsNullOrWhiteSpace(fingno.Text)&&string.IsNullOrWhiteSpace(headpic.Source.ToString()))
            {
                MessageDialog.ShowDialog("卡号、指纹、人脸不能为同时为空");
                return;
            }
            //faceBase64 = headpic.Source.ToString();
            var biz = new CommBiz();
            if (faceBytes != null && faceBytes.Length > 0)
            {
                //删除原先的face
                var result = BaiduFaceApiHelper.GetInstance().Contrast(faceBytes);
                if (result.result)
                {
                    if (faceId == result.data)
                    {
                        BaiduFaceApiHelper.GetInstance().UpdateFace(faceId, faceBytes);
                        faceBase64 = Convert.ToBase64String(faceBytes);
                    }
                    else
                    {
                        //去数据库查询人脸ID是否存在 如果存在不让添加  不存在
                        //var isf = biz.IsFace(result.data);
                        //if (isf)
                        //{
                        //    MessageDialog.ShowDialog("人脸已存在，无法继续添加");
                        //    return;
                        //}
                        //else
                        //{
                        //    BaiduFaceApiHelper.GetInstance().DeleteFace(result.data);
                           
                        //    var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                        //    if (res.result)
                        //    {
                        //        faceId = res.data;
                        //        faceBase64 = Convert.ToBase64String(faceBytes);
                        //    }
                        //}
                    }
                }
                else
                {
                    var res = BaiduFaceApiHelper.GetInstance().EditUserWithDB(faceBytes, faceId);
                    if (res.result)
                    {
                        faceId = res.data;
                        faceBase64 = Convert.ToBase64String(faceBytes);
                        //MessageBox.Show(faceId + ":人脸id");
                        //MessageBox.Show("人脸编码:" + faceBytes.ToString());
                    }
                }
            }

            this.DialogResult = true;
        }

        /// <summary>
        /// 扫描卡号
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            try
            {
                CardNo no = new CardNo();
                var result = no.ShowDialog();
                if (result != true)
                {
                    return;
                }
                code.Text = no.TextBox1.Text;
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }

        /// <summary>
        /// 人脸识别
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click4(object sender, RoutedEventArgs e)
        {
            try
            {
                Face fa = new Face();
                var result = fa.ShowDialog();
                if (result != null && result.Value)
                {
                    faceBytes = (byte[])fa.Facebytes.Clone();
                    headpic.Source = (BitmapSource)new ImageSourceConverter().ConvertFrom(faceBytes);
                    faceBase64 = Convert.ToBase64String(faceBytes);
                    //面部识别成功后展示面部识别码
                    //MessageDialog.ShowDialog(faceBase64);

                }
            }
            catch (Exception exception)
            {
                TextLogUtil.Info(exception.Message);
                MessageDialog.ShowDialog(exception.Message);
            }
        }

        private void Button1_Click41(object sender, RoutedEventArgs e)
        {

           

        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.DragMove();
        }

        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ComWindow.Focus(sender);
            e.Handled = true;
        }


        //指纹录入
        private void Button_Finer(object sender, RoutedEventArgs e)
        {
            try
            {
                Finger finger = new Finger();
                var result = finger.ShowDialog();
                if (result != true)
                {
                    return;
                }
                fingno.Text = finger.Features;
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }
    }
}

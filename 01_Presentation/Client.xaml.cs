﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DJ.Clients;
using DJ.Dialog;
using DJ.Third;
using DJ.ZEF.ThirdParty;
using NSJL.Biz.Background.Client;
using NSJL.Biz.Background.Quartzs;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.Client;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;

namespace DJ
{
    /// <summary>
    /// Client.xaml 的交互逻辑
    /// </summary>
    public partial class Client : Window
    {
        CommBiz biz = new CommBiz();
        IniFile ini = new IniFile("Config/Config.ini");
        public Client()
        {
            InitializeComponent();
            ComWindow.Start(this);
            TextLogUtil.Info("启动成功");
            lb1.Content = ini.readKey("MainCompanyShow", "Company");
            lb2.Content = ini.readKey("MainCompanyShow", "Phone");
            Task.Run(() =>
            {
                try
                {
                    #region 锁板串口开启,只启动一次
                    var ini = new IniFile("Config/Config.ini");
                    var portName = ini.readKey("BoxConfig", "portName");
                    var baudRate = ini.readKey("BoxConfig", "baudRate");
                    var result = LockManager.GetInstance().Start(portName, baudRate.ToInt32());
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }
                    #endregion
                    #region 初始化人脸识别设备
                    var common = BaiduFaceApiHelper.GetInstance().Start();
                    if (!common.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(common.message);
                        });
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                }
            });
        }


        private void mouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Login login = new Login();
                login.ShowDialog();
                lb1.Content = ini.readKey("MainCompanyShow", "Company");
                lb2.Content = ini.readKey("MainCompanyShow", "Phone");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }


        //存物
        private void btn_Cun(object sender, RoutedEventArgs e)
        {
            UserCheck uc = new UserCheck();
            uc.ShowDialog();
        }

        //取物
        private void btn_Qu(object sender, RoutedEventArgs e)
        {
            UserCheck uc = new UserCheck();
            uc.ShowDialog();
        }
    }
}

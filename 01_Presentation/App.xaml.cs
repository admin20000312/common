﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using NSJL.Framework.Utils;

namespace DJ
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            string MName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            string PName = System.IO.Path.GetFileNameWithoutExtension(MName);
            System.Diagnostics.Process[] myProcess = System.Diagnostics.Process.GetProcessesByName(PName);

            if (myProcess.Length > 1)
            {
                MessageBox.Show("本程序一次只能运行一个实例！", "提示");
                Application.Current.Shutdown();
                Environment.Exit(0);
                return;
            }

            DispatcherUnhandledException += App_DispatcherUnhandledException;
            base.OnStartup(e);


            if (MName == "SDJ.exe")
            {
                //服务端
                Application.Current.StartupUri = new Uri("/Login.xaml", UriKind.Relative);
            }
            else
            {
                //客户端   30分钟轮训  开关报警器    1分钟1一次同步服务端的配置开启动作
            }


            //Change("BJ-TechnologyBlue");

        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            TextLogUtil.Info("程序异常：" + e.Exception.Source + "@@" + e.Exception.Message);
        }



       
    }
}

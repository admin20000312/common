﻿using DJ.ZEF.ThirdParty;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.UserControls
{
    /// <summary>
    /// NowTime.xaml 的交互逻辑
    /// </summary>
    public partial class NowData : UserControl
    {
        ResourceDictionary mergedDictionaries = null;
        public NowData()
        {
           
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0,0,0,0,500);
            timer.Tick += ((a, b) =>
            {
                mergedDictionaries = Application.Current.Resources.MergedDictionaries[0];
                var txt = DateTime.Now.ToString(@"yyyy \\ MM \\ dd ");
                var str = FindResource("International").ToString();
                if (str == "English")
                {
                    Label1.Content = txt + "  " + DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("en-US"));
                }
                else if(str == "中文")
                {
                    Label1.Content = txt + "  " + DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("zh-cn"));
                }else
                {
                    Label1.Content = txt + "  " + DateTime.Now.ToString("dddd", new System.Globalization.CultureInfo("ja-JP"));
                }
            });
            timer.IsEnabled = true;
            timer.Start();

        }
    }
}

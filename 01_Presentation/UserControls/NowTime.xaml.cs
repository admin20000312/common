﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.UserControls
{
    /// <summary>
    /// NowTime.xaml 的交互逻辑
    /// </summary>
    public partial class NowTime : UserControl
    {
        public NowTime()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                time.Content = DateTime.Now.ToString("HH : mm : ss");
            });
            timer.IsEnabled = true;
            timer.Start();

        }
    }
}

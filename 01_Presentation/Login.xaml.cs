﻿using DJ.Clients;
using DJ.Dialog;
using NPOI.SS.Formula.Functions;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ
{
    /// <summary>
    /// Login1.xaml 的交互逻辑
    /// </summary>
    public partial class Login : Window
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public Login()
        {
            InitializeComponent();
            ComWindow.Start(this, lab1);
            MName = Process.GetCurrentProcess().MainModule.ModuleName;
        }

        //返回
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (MName == "SDJ.exe")
            {
                try
                {
                    Environment.Exit(0);
                }
                catch (Exception exception)
                {
                    TextLogUtil.Info(exception.Message);
                    Environment.Exit(0);
                }
            }
            else
            {
                this.Close();
            }
        }

        public string MName { get; set; } = "DJ.exe";
        private void Btn_Login(object sender, RoutedEventArgs e)
        {
            bool isSystem = ini.readKey("SystemPwd", "IsUsing").ToInt32() == 1 ? true:false;
            Label1.Content = "";
            var account = TextBox1.Text;
            var passowrd = PasswordBox1.Password;
            string pwd = passowrd.ToLower();
            Task.Run(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    if (!isSystem)
                    {
                        loginCommon(account,pwd);
                    }
                    else //超管动态密码
                    {
                        if (account == "dongjie001")
                        {
                            string time = DateTime.Now.ToString("MMdd");
                            string checkPwd = "dongjie" + SelfUtil.StringToHexString(time).ToLower();
                            if (pwd == checkPwd)
                            {
                                Main.UserName = "超级管理员";
                                Main.Permission = "超级管理员";
                                try
                                {
                                    //客户端
                                    Main main = Main.GetInstance();
                                    main.ShowDialog();
                                    this.Hide();
                                }
                                catch (Exception exception)
                                {
                                    TextLogUtil.Info("登录异常：" + exception.Message);
                                    Label1.Content = exception.Message;
                                    Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                                }
                            }
                            else
                            {
                                Label1.Content = "登录异常";
                                Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                            }
                        }
                        else
                        {
                            loginCommon(account, pwd);
                        }
                    }
                });
            });
        }

        public void loginCommon(string account,string pwd)
        {
            var biz = new CommBiz();
            var result = biz.Login(account, pwd);
            if (result.result)
            {
                Main.UserName = result.data.UserName;
                Main.Permission = result.data.Permission;
                try
                {
                    //服务端
                    if (MName == "SDJ.exe")
                    {
                    }
                    else
                    {
                        //客户端
                        Main main = Main.GetInstance();
                        main.ShowDialog();
                        this.Hide();
                    }
                }
                catch (Exception exception)
                {
                    TextLogUtil.Info("登录异常：" + exception.Message);
                    Label1.Content = exception.Message;
                    Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    //MessageDialog.ShowDialog(result.message);
                    //MessageBox.Show(result.message);
                    Label1.Content = result.message;
                    Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                });
            }
        }

        private void Button_Other(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Tag.ToString() == "1")
            {
                panel1.Visibility = Visibility.Visible;
                btn.Tag = "0";
            }
            else
            {
                panel1.Visibility = Visibility.Hidden;
                btn.Tag = "1";
            }
        }


        private void TextBox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Label1.Content ="请输入账号密码";
            Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#EFF3F6");
            ComWindow.Focus(sender);
            e.Handled = true;
        }
    }
}

﻿using DJ.Clients.Background;
using DJ.Dialog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main : Window
    {
        public static string UserName = null;
        public static string Permission = null;
        public Main()
        {
            InitializeComponent();  
        }

        private static readonly object locks = new object();
        private static Main manage;
        public static Main GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new Main());
            }
        }

        //返回
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        //系统设置
        private void btn_System(object sender, RoutedEventArgs e)
        {
            if (Permission != "超级管理员")
            {
                MessageDialog.ShowDialog("您没有超管权限");
                return;
            }
            SystemView sv = new SystemView();
            sv.ShowDialog();
        }

       
        //箱格管理
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BoxView bv = new BoxView();
            bv.ShowDialog();
        }


        //日志记录
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            LogView lv = new LogView();
            lv.ShowDialog();
        }


        //管理员设置
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (Permission != "超级管理员")
            {
                MessageDialog.ShowDialog("您没有超管权限");
                return;
            }
            ManagerSettingView msv = new ManagerSettingView();
            msv.ShowDialog();
        }


        //用户管理
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            UserManagerView umv = new UserManagerView();
            umv.ShowDialog();
        }


        //退出系统
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            ExitSystem es = new ExitSystem();
            if (es.ShowDialog() == true)
            {
                this.Hide();
            }
        }

        //当前登录管理员密码设置
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            AdminPwdSetView apv = new AdminPwdSetView();
            if(apv.ShowDialog() == true)
            {
                this.Hide();
            }
        }

    

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Permission != "超级管理员")
            {
                managerBtn.Visibility = Visibility.Collapsed;
                systemBtn.Visibility = Visibility.Collapsed;
            }
            else
            {
                managerBtn.Visibility = Visibility.Visible;
                systemBtn.Visibility = Visibility.Visible;
            }
        }
    }
}

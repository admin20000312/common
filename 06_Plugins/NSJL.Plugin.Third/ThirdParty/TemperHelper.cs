﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{
    public class TemperHelper
    {
        private static readonly object locks = new object();
        private static TemperHelper manage;
        public static TemperHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new TemperHelper());
            }
        }
        public Action<string> CallBackStr = null;
        private SerialPortHelp sp = null;
        // 秒数  * 10的值
        private int timeOut { get; set; } = 20;
        // 当前值/10  为发送秒数间隔
        private int timeOutSpace { get; set; } = 10;
        private CachedBuffer cache = new CachedBuffer();
        public CommonResult Start(string portName, int baudRate,Action<string> action)
        {
            CallBackStr = action;
            //var data = SelfUtil.GetXmlWithPath<TemHumMod>("config/TemHumCfg.xml");

            if (sp == null || !sp.sp.IsOpen)
            {
                sp = new SerialPortHelp();
                sp.CallBackAction = DataReceived;
                return sp.Start(portName, baudRate);
            }
            return new CommonResult() { result = true };
        }

        public void GetData(Action<double,double> callback)
        {
            var bytes = new byte[] { 0x01, 0x03, 0x00,0x00,0x00,0x02,0xC4,0x0b };

            Task.Factory.StartNew(() =>
            {
                var flag = 0;
                while (callBackResult == null)
                {
                    if (flag % timeOutSpace == 0)
                    {
                        sp.SendByte(bytes);
                    }
                    Thread.Sleep(100);
                    if (flag > timeOut)
                    {
                        break;
                    }
                    flag++;
                }
                if (callBackResult != null)
                {
                    //第4 第5 个字节是湿度
                    double temperature = ((callBackResult[3] << 8) + callBackResult[4]) / 10.0;
                    //第6 第7 个字节是温度
                    double humidity;
                    //判断正负
                    if (callBackResult[5] >> 7 == 1)
                    {
                        humidity = -(0x10000 - (callBackResult[5] << 8) - callBackResult[6]) / 10.0;
                    }
                    else
                    {
                        humidity = ((callBackResult[5] << 8) + callBackResult[6]) / 10.0;
                    }
                    callback(temperature, humidity);
                    callBackResult = null;
                    return;
                }
                callback(0,0);
                callBackResult = null;
            });
        }
        private byte[] callBackResult = null;
        public void DataReceived(byte[] buffer)
        {
            //温度计协议未写
            //cache.Write(buffer, 0, buffer.Length);
            //var temp = cache.ReadDataWithCard(CardType);
            //if (temp == null)
            //{
            //    return;
            //}

            callBackResult = new byte[buffer.Length];
            Array.Copy(buffer, callBackResult, buffer.Length);
        }



    }
}

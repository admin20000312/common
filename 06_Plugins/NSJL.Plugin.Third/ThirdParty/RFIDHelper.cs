﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using RFIDReaderNetwork_SerialSDK_ForCSharp.DataStructureLayer;
using RFIDReaderNetwork_SerialSDK_ForCSharp.ExternalInterfaceLayer;

namespace DJ.ZEF.ThirdParty
{
    public class RFIDHelper
    {
        private static readonly object locks = new object();
        private static RFIDHelper manage;
        public static RFIDHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new RFIDHelper());
            }
        }
        public Action<string, int> CallBack;
        public Action<bool, string> OnSuccess;

        private RFIDClient rfidClientReader;

        private RFIDServer rifdServer;
        private RFIDServerModel rifdServerModel;

        public OperatReader m_rfidWorkReader { get; set; }

        //   0  表示串口    1表示   TCP
        public int SelectType { get; set; }
        //  1  表示成功
        public int IsSuccess { get; set; }

        public CommonResult ConnectNetWork(Action<bool, string> temp1,Action<string, int> temp2, int port = 7880)
        {
            SelectType = 1;
            OnSuccess = temp1;
            CallBack = temp2;


            rifdServer = new RFIDServer(port);
            var nRetVal = rifdServer.StartServer();                      //开启服务
            if (nRetVal == OperationResult.SUCCESS)
            {
                rifdServer.m_OnErrorOccured += new EventHandler<ErrorReportEventArgs>(onErrorOccuredEven);
                rifdServer.m_OnRegistered += new EventHandler<RegisteredEventArgs>(m_rifdServer_m_OnRegistered);
                rifdServer.m_OnUnregistered += new EventHandler<UnregisteredEventArgs>(m_rifdServer_m_OnUnregistered);
                rifdServer.m_OnInventoryReport += new EventHandler<InventoryReportEventArgs>(onInventoryReport);

                return new CommonResult(){result = true,message = "OK"};
            }
            else
            {
                TextLogUtil.Info("初始化UHF失败！错误码是 " + port + nRetVal.ToString() + "--------UHF设备错误");
                return new CommonResult(){result = true,message = nRetVal.ToString() };
            }
        }
        public void ConnectCOM(string portName,int baudRate=115200)
        {
            //var filepath = AppDomain.CurrentDomain.BaseDirectory + "config/WBLUHFConfig.ini";
            //var ini = new IniFile(filepath);
            //var portName = ini.readKey("WBLUHFInfo", "portName");
            //var baudRate = Convert.ToInt32(ini.readKey("WBLUHFInfo", "baudRate"));
            SelectType = 0;

            if (rfidClientReader != null)
            {
                rfidClientReader.Disconnect();
            }
            rfidClientReader = new RFIDClient();
            OperationResult result = rfidClientReader.ConnectSerial(portName.ToUpper(),baudRate);
            if (result != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("初始化UHF失败！错误码是 " + portName + result.ToString()+"--------UHF设备错误");
                return;
            }
            //断开连接
            rfidClientReader.m_OnDisconnect += new EventHandler<DisconnectEventArgs>(onDisconnectEven);
            //报错的
            rfidClientReader.m_OnErrorcallback += new EventHandler<ErrorReportEventArgs>(onErrorOccuredEven);
            //盘点的时候
            rfidClientReader.m_OnInventoryReport += new EventHandler<InventoryReportEventArgs>(onInventoryReport);

            m_rfidWorkReader = rfidClientReader;

            IsSuccess = 1;
        }

        //串口模式

        //盘点事件通用
        private void onInventoryReport(object sender, InventoryReportEventArgs e)
        {
            if (CallBack != null)
            {
                CallBack(e.m_stInventoryResult.m_strEPC ,e.m_stInventoryResult.m_strAntennaNo.ToInt32());
            }
        }
        //错误事件通用
        private void onErrorOccuredEven(object sender, ErrorReportEventArgs e)
        {
            TextLogUtil.Info(e.m_strErrorCode.ToString()+ "------UHF设备错误");
        }
        private void onDisconnectEven(object sender, DisconnectEventArgs e)
        {
            IsSuccess = 0;
            TextLogUtil.Info("UHF设备失去连接----UHF设备错误");
            //Start();
        }

        //网络 TCP 模式
        //链接成功
        private void m_rifdServer_m_OnRegistered(object sender, RegisteredEventArgs e)
        {
            if (e != null)
            {
                rifdServerModel = new RFIDServerModel(e.m_strDeviceID);
                m_rfidWorkReader = rifdServerModel;

                //同步时间
                DateTime day = DateTime.Now;
                int month = day.Month;
                int d = day.Day;
                int h = day.Hour;
                int mm = day.Minute;
                int y = day.Year;
                int s = day.Second;
                string time;
                time = String.Format("{0:D2}{1:D2}{2:D2}{3:D2}{4:D4}.{5:D2}", month, d, h, mm, y, s);
                var nRetVal = rifdServerModel.SetCurrentTime(time);
                if (nRetVal == OperationResult.SUCCESS)
                {
                    IsSuccess = 1;
                    OnSuccess?.Invoke(true,"连接成功");
                }
                else
                {
                    TextLogUtil.Info(nRetVal.ToString());
                    OnSuccess?.Invoke(false, nRetVal.ToString());
                }
            }
        }
        //断开链接
        private void m_rifdServer_m_OnUnregistered(object sender, UnregisteredEventArgs e)
        {
            IsSuccess = 0;
            TextLogUtil.Info("UHF设备失去连接----UHF设备错误");
        }

        public CommonResult StartRead()
        {
            if (IsSuccess != 1)
            {
                return new CommonResult() { result = false, message = "设备未连接" };
            }
            try
            {
                OperationResult result = m_rfidWorkReader.StartPerioInventory();
                if (result != OperationResult.SUCCESS)
                {
                    TextLogUtil.Info("UHF设备读取数据命令失败！错误码是" + result.ToString());
                    return new CommonResult() { result = false, message = result.ToString() };
                }
                return new CommonResult() { result = true };
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
                return new CommonResult() { result = false, message =e.Message };
            }
        } 
        public CommonResult StopRead()
        {
            if (IsSuccess != 1)
            {
                return new CommonResult(){result = false,message = "设备未连接"};
            }
            try
            {
                OperationResult result = m_rfidWorkReader.StopPeriodInventory();
                if (result != OperationResult.SUCCESS)
                {
                    TextLogUtil.Info("UHF设备停止读取数据命令失败！错误码是" + result.ToString());
                    return new CommonResult() { result = false, message = result.ToString() };
                }
                return new CommonResult(){result = true};
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
                return new CommonResult() { result = false, message = e.Message };
            }
        }

        //GetHummer   蜂鸣器   开关  1 启用   0  禁用
        public void StartHummer(int state=1)
        {
            Hummer hummerInfo = new Hummer();
            UInt32 data = 0xff;
            OperationResult nRetVal = OperationResult.FAIL;
            hummerInfo.State = state;
            nRetVal = m_rfidWorkReader.GetHummer(ref hummerInfo);
            if (nRetVal != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("设置蜂鸣器失败：原因:"+ nRetVal.ToString());
            }
               
            nRetVal = m_rfidWorkReader.SetLed(1, data);
            if (nRetVal != OperationResult.SUCCESS)
            {
                TextLogUtil.Info("设置蜂鸣器失败：原因:" + nRetVal.ToString());
            }
        }


        //传入 天线ID  循环盘点5次  返回:盘到的标签和天线号
        public List<RFIDInfo> StartOnceReadByAnt(int antId,int Num=5)
        {
            if (IsSuccess != 1)
            {
                return null;
            }
            TextLogUtil.Info("开始盘点天线号："+antId);
            var list = new List<RFIDInfo>();
            for (var j = 0; j < Num; j++)
            {
                var tagReport = new TagReport();
                OperationResult result = m_rfidWorkReader.Inventory(antId, ref tagReport);
                if (result == OperationResult.SUCCESS)
                {
                    for (var z = 0; z < tagReport.m_listTags.Count; z++)
                    {
                        var info=new RFIDInfo();
                        info.LabelId = tagReport.m_listTags[z].m_strEPC;
                        info.AntennaNo= tagReport.m_listTags[z].m_nAntennaNo.ToString();
                        if (!list.Contains(info))
                        {
                            TextLogUtil.Info("天线号：" + info.AntennaNo + "盘点到：" + info.LabelId);
                            list.Add(info);
                        }
                    }
                }
            }
            TextLogUtil.Info("结束盘点天线号：" + antId);
            return list;
        }
        public class RFIDInfo
        {
            //标签号
            public string LabelId { get; set; }
            //天线号
            public string AntennaNo { get; set; }
        }

        public void Close()
        {
            StopRead();

            if (IsSuccess != 1)
            {
                return;
            }
            try
            {
                if (SelectType == 0)
                {
                    if (rfidClientReader != null)
                    {
                        var nRetVal = rfidClientReader.Disconnect();
                        if (nRetVal != OperationResult.SUCCESS)
                        {
                            TextLogUtil.Info("断开读写器失败,原因可能为" + nRetVal.ToString());
                            return;
                        }
                        rfidClientReader = null;
                    }
                }
                else if (SelectType == 1)
                {
                    if (rifdServer != null)
                    {
                        rifdServer.StopServer();
                        rifdServerModel = null;
                        rifdServer = null;
                    }
                }
                CallBack = null;
                m_rfidWorkReader = null;
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }

    }
}

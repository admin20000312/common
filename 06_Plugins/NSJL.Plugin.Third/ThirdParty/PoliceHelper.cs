﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Plugin.Third.ThirdParty
{

    /// <summary>
    /// 报警器
    /// </summary>
    public class PoliceHelper
    {
        private static readonly object locks = new object();
        private static PoliceHelper manage;
        public static PoliceHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new PoliceHelper());
            }
        }
        private SerialPortHelp sp = null;
        public CommonResult Start(string portName, int baudRate)
        {
            if (sp == null)
            {
                sp = new SerialPortHelp();
                return sp.Start(portName, baudRate);
            }
            return new CommonResult() { result = false, message = "报警器初始化错误" };
        }
        // 1表示开启     0 表示关闭
        public void SendByte(int type=1,int number=1)
        {
            try
            {
                byte[] bytes = null;
                if (type == 1)
                {
                    bytes = new byte[] { 0x55, (byte)number, 0xb1, 0x5f, 0x00 };
                }
                else
                {
                    bytes = new byte[] { 0x55, (byte)number, 0xb2, 0x5f, 0x00 };
                }
                //计算校验位
                var temp = SelfUtil.XorCheck(bytes);
                var newbyte = new byte[bytes.Length + 1];
                Array.Copy(bytes, newbyte, bytes.Length);
                newbyte[bytes.Length] = temp;

                sp.SendByte(newbyte);
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }


    }
}

﻿using DJ.ZEF.ThirdParty;
using NSJL.DomainModel.Background.System;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static DJ.ZEF.ThirdParty.BaiduFaceApiHelper;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class Video2Helper
    {
        private static readonly object locks = new object();
        private static Video2Helper manage;
        public static Video2Helper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new Video2Helper());
            }
        }

        public void Stop()
        {
            tokenSource.Cancel();
        }
        CancellationTokenSource tokenSource = new CancellationTokenSource();
        public bool IsBuys = false;
        public CommonResult Start(Action<Bitmap> bitaction, Action<byte[]> action)
        {
            try
            {
                tokenSource = new CancellationTokenSource();
                CancellationToken token = tokenSource.Token;

                Task.Run(() =>
                {
                    using (VideoCapture cap = VideoCapture.FromCamera(0))
                    {
                        try
                        {
                            Mat mat = new Mat();
                            while (!token.IsCancellationRequested)
                            {
                                cap.Read(mat);

                                if (!mat.Empty())
                                {
                                    int ilen = 7;//传入的人脸数
                                    TrackFaceInfo[] track_info = new TrackFaceInfo[ilen];
                                    for (int i = 0; i < ilen; i++)
                                    {
                                        track_info[i] = new TrackFaceInfo();
                                        track_info[i].landmarks = new int[144];
                                        track_info[i].headPose = new float[3];
                                        track_info[i].face_id = 0;
                                        track_info[i].score = 0;
                                    }
                                    int sizeTrack = Marshal.SizeOf(typeof(TrackFaceInfo));
                                    IntPtr ptT = Marshal.AllocHGlobal(sizeTrack * ilen);

                                    int faceSize = ilen;//返回人脸数  分配人脸数和检测到人脸数的最小值
                                    int curSize = ilen;//当前人脸数 输入分配的人脸数，输出实际检测到的人脸数
                                    faceSize = BaiduFaceApiHelper.GetInstance().TrackMat(ptT, mat.CvPtr, ref curSize);

                                    for (int index = 0; index < faceSize; index++)
                                    {
                                        IntPtr ptr = new IntPtr();
                                        if (8 == IntPtr.Size)
                                        {
                                            ptr = (IntPtr)(ptT.ToInt64() + sizeTrack * index);
                                        }
                                        else if (4 == IntPtr.Size)
                                        {
                                            ptr = (IntPtr)(ptT.ToInt32() + sizeTrack * index);
                                        }
                                        track_info[index] = (TrackFaceInfo)Marshal.PtrToStructure(ptr, typeof(TrackFaceInfo));

                                        if (track_info[index].score >= 0.85)
                                        {
                                            if (IsBuys)
                                            {
                                                continue;
                                            }
                                            IsBuys = true;

                                            var temp1 = mat.ToMemoryStream();
                                            System.Drawing.Bitmap imgs1 = (Bitmap)System.Drawing.Image.FromStream(temp1);
                                            var bitmapBytes = ImageToBytes(imgs1);
                                            if (bitmapBytes == null)
                                            {
                                                continue;
                                            }
                                            Task.Run(() =>
                                            {
                                                var result = BaiduFaceApiHelper.GetInstance().Verification(bitmapBytes);
                                                if (result.result)
                                                {
                                                    //判断人脸是否存在
                                                    var temps = BaiduFaceApiHelper.GetInstance().Contrast(bitmapBytes);
                                                    if (!temps.result)
                                                    {
                                                        //添加人脸
                                                        temps = BaiduFaceApiHelper.GetInstance().EditUserWithDB(bitmapBytes);
                                                    }
                                                    action(bitmapBytes);
                                                    Stop();
                                                }
                                                IsBuys = false;
                                            });

                                            var box = bounding_box(track_info[index].landmarks, track_info[index].landmarks.Length);
                                            draw_rotated_box(ref mat, ref box, new Scalar(0, 255, 0));
                                        }
                                    }

                                    var temp = mat.ToMemoryStream();
                                    System.Drawing.Bitmap imgs = (Bitmap)System.Drawing.Image.FromStream(temp);
                                    temp.Close();
                                    temp.Dispose();
                                    bitaction(imgs);

                                    Marshal.FreeHGlobal(ptT);
                                    Cv2.WaitKey(1);

                                    //测试保存人脸图片
                                    //var logPath = string.Format("{0}/temp/{1}.jpg", Environment.CurrentDirectory, ts);
                                    //Cv2.ImWrite(logPath, mat);
                                }
                            }
                            mat.Release();
                        }
                        catch (Exception e)
                        {
                            IsBuys = false;
                            //MessageBox.Show(e.Message);
                        }
                    }
                }, token);


                return new CommonResult(){result = true};
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false };
            }
        }

        
        public static byte[] ImageToBytes(Bitmap image)
        {
            try
            {
                if (image == null)
                {
                    return null;
                }
                MemoryStream ms = new MemoryStream();
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] bytes = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(bytes, 0, (int)ms.Length);
                ms.Close();
                return bytes;
            }
            catch (Exception ep)
            {
                return null;
            }
        }

        public RotatedRect bounding_box(int[] landmarks, int size)
        {
            int min_x = 1000000;
            int min_y = 1000000;
            int max_x = -1000000;
            int max_y = -1000000;
            for (int i = 0; i < size / 2; ++i)
            {
                min_x = (min_x < landmarks[2 * i] ? min_x : landmarks[2 * i]);
                min_y = (min_y < landmarks[2 * i + 1] ? min_y : landmarks[2 * i + 1]);
                max_x = (max_x > landmarks[2 * i] ? max_x : landmarks[2 * i]);
                max_y = (max_y > landmarks[2 * i + 1] ? max_y : landmarks[2 * i + 1]);
            }
            int width = ((max_x - min_x) + (max_y - min_y)) / 2;
            float angle = 0;
            Point2f center = new Point2f((min_x + max_x) / 2, (min_y + max_y) / 2);
            return new RotatedRect(center, new Size2f(width, width), angle);
        }
        public void draw_rotated_box(ref Mat img, ref RotatedRect box, Scalar color)
        {
            Point2f[] vertices = new Point2f[4];
            vertices = box.Points();
            for (int j = 0; j < 4; j++)
            {
                var xxx = vertices[j];
                var ccc = vertices[(j + 1) % 4];

                //Cv2.Line(img, vertices[j], vertices[(j + 1) % 4], color);
                Cv2.Line(img, Convert.ToInt32(xxx.X), Convert.ToInt32(xxx.Y), Convert.ToInt32(ccc.X), Convert.ToInt32(ccc.Y), color);
            }
        }


    }

}

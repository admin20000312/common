﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace DJ.ZEF.ThirdParty
{
    public class BaiduFaceApiHelper
    {
        private static string group_id = "dj";
        private static readonly object locks = new object();
        private static BaiduFaceApiHelper manage;
        public static BaiduFaceApiHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new BaiduFaceApiHelper());
            }
        }
        public CommonResult Start()
        {
            // id_card=false表示采用生活照模式（通常采用的模式）
            // id_card=true表示采用证件照模式（如识别身份证等小图）和生活照模式不能混用
            bool idcard = false;
            // model_path为模型文件夹路径，即face-resource文件夹（里面存的是人脸识别的模型文件）
            // 传空为采用默认路径，若想定置化路径，请填写全局路径如：d:\\model
            // 若模型文件夹采用定置化路径，则激活文件(license.ini, license.key)也需要放置到该目录，否则激活文件会找不到路径
            string path = AppDomain.CurrentDomain.BaseDirectory+ "face-resource";
            TextLogUtil.Info(path);
            int n = sdk_init(null, idcard);
            TextLogUtil.Info("初始化结果===================："+n);
            if (n != 0)
            {
                TextLogUtil.Info("人脸SDK初始化失败");
                return new CommonResult(){result = false,message = "人脸SDK初始化失败"};
            }

            
            bool authed = is_auth();
            if (!authed)
            {
                TextLogUtil.Info("人脸SDK未激活,请点击根目录exe输入激活码");
                return new CommonResult() { result = false, message = "人脸SDK未激活,请点击根目录exe输入激活码" };
            }

            //销毁
            //sdk_destroy();
            return new CommonResult(){result = true, message = "人脸识别激活成功" };
        }


        private static readonly object objects=new object();

        public CommonResult Verification(byte[] image)
        {
            lock (objects)
            {
                //验证属性
                IntPtr ptr = face_attr_by_buf(image, image.Length);
                string buf = Marshal.PtrToStringAnsi(ptr);
                Console.WriteLine("00000000000000000000000000000000000000000000"+buf);
                var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfoS>(buf);
                if (info.errno != 0)
                {
                    //TextLogUtil.Info("buf:"+buf);
                    return new CommonResult() { result = false, message = info.msg };
                }
                //验证质量
                IntPtr ptr1 = face_quality_by_buf(image, image.Length);
                string buf1 = Marshal.PtrToStringAnsi(ptr1);

                var info1 = new JavaScriptSerializer().Deserialize<FaceAttributeInfoS>(buf1);
                if (info1.errno != 0)
                {
                    //TextLogUtil.Info("buf1:" + buf1);
                    return new CommonResult() { result = false, message = info1.msg };
                }
                //验证活体  需要判断分数
                IntPtr ptr2 = rgb_liveness_check_by_buf(image, image.Length);
                string buf2 = Marshal.PtrToStringAnsi(ptr2);
                var info2 = new JavaScriptSerializer().Deserialize<FaceAttributeInfoS>(buf2);
               
                if (info2.errno != 0)
                {
                    //TextLogUtil.Info("buf2:" + buf2);
                    return new CommonResult() { result = false, message = info2.msg };
                }
                if (info2.data.score < 0.6M)
                {
                    TextLogUtil.Info("验证活体分数不到60");
                    return new CommonResult() { result = false, message = "验证活体分数不到60" };
                }
                return new CommonResult() { result = true };
            }
        }

        //录入db文件
        public CommonResult EditUserWithDB(byte[] image,string userid=null)
        {
            lock (objects)
            {
                try
                {
                    //TextLogUtil.Info("33333333333333333333333333");
                    if (string.IsNullOrWhiteSpace(userid))
                    {
                        //设置faceID
                        userid = ConvertDataTime2Long(DateTime.Now).ToString();
                        TextLogUtil.Info($"用户的面部id==================:{userid}");
                    }

                    //人脸注册(传入图片二进制buffer)
                    IntPtr ptr = user_add_by_buf(userid, group_id, image, image.Length);
                    var buf = Marshal.PtrToStringAnsi(ptr);
                    
                    var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfoS>(buf);
                    TextLogUtil.Info(info.msg);
                    if (info.msg != "success")
                    {
                        //TextLogUtil.Info(buf);
                        return new CommonResult() { result = false, message = info.msg };
                    }
                    return new CommonResult() { result = true, data = userid };
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                    return new CommonResult() { result = false, message = e.Message };
                }
            }
        }
        //判断原始数据库 是否存在  就不需要录入db文件 
        public CommonResult Contrast(byte[] image)
        {
            lock (objects)
            {
                try
                {
                    //提前加载库里所有数据到内存中
                    load_db_face();
                    //1:N人脸识别（传人脸图片文件和内存已加载的整个库数据比对）
                    IntPtr ptr = identify_by_buf_with_all(image, image.Length);
                    var buf = Marshal.PtrToStringAnsi(ptr);
                    var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfo1>(buf);
                    List<FaceGroupResult> result = info.data.result;
                    foreach(var id in result) 
                    {
                        TextLogUtil.Info($"用户id:{id.user_id}");
                    }
                    if (info.msg != "success")
                    {
                        TextLogUtil.Info(buf);
                        return new CommonResult() { result = false, message = info.msg+"111111" };
                    }
                    if (info.data.result.Count > 0)
                    {
                        var user = info.data.result.OrderByDescending(p => p.score).FirstOrDefault();
                        if (user != null && user.score > 80M)
                        {
                            return new CommonResult() { result = true, message = "人脸已存在", data = user.user_id };
                        }
                    }
                }
                catch (Exception e)
                {
                    return new CommonResult() { result = false, message = e.Message };
                }
                return new CommonResult() { result = false, message = "人脸不存在" };
            }
        }

        public CommonResult DeleteFace(string userid)
        {
            lock (objects)
            {
                try
                {
                    IntPtr ptr = user_delete(userid, group_id);
                    var buf = Marshal.PtrToStringAnsi(ptr);
                    var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfo>(buf);
                    if (info.msg != "success")
                    {
                        TextLogUtil.Info(buf);
                        return new CommonResult() { result = false, message = info.msg };
                    }
                    return new CommonResult() { result = true };
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                    return new CommonResult() { result = false, message = e.Message };
                }
            }
            
        }
        public CommonResult UpdateFace(string userid, byte[] image)
        {
            lock (objects)
            {
                try
                {
                    IntPtr ptr = user_update_by_buf(userid, group_id, image, image.Length);
                    var buf = Marshal.PtrToStringAnsi(ptr);
                    var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfo>(buf);
                    if (info.msg != "success")
                    {
                        TextLogUtil.Info(buf);
                        return new CommonResult() { result = false, message = info.msg };
                    }
                    return new CommonResult() { result = true };
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                    return new CommonResult() { result = false, message = e.Message };
                }
            }
            
        }

        public CommonResult<List<string>> GetAllUserList()
        {
            lock (objects)
            {
                try
                {
                    IntPtr ptr = get_user_list(group_id, 0, 1000);
                    var buf = Marshal.PtrToStringAnsi(ptr);
                    var info = new JavaScriptSerializer().Deserialize<FaceAttributeInfo>(buf);
                    if (info.msg != "success")
                    {
                        TextLogUtil.Info(buf);
                        return new CommonResult<List<string>>() { result = false, message = info.msg };
                    }
                    var list = info.data.user_id_list.Select(p => p.user_id).ToList();

                    return new CommonResult<List<string>>() { result = true, data = list };
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                    return new CommonResult<List<string>>() { result = false, message = e.Message };
                }
            }
            
        }
        public int TrackMat(IntPtr oface, IntPtr mat, ref int max_track_num)
        {
            lock (objects)
            {
                return track_mat(oface, mat, ref max_track_num);
            }
        }




        [DllImport("BaiduFaceApi.dll", EntryPoint = "track_mat", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern int track_mat(IntPtr oface, IntPtr mat, ref int max_track_num);
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct FaceInfo
        {
            public FaceInfo(float iWidth, float iAngle, float iCenter_x, float iCenter_y, float iConf)
            {
                mWidth = iWidth;
                mAngle = iAngle;
                mCenter_x = iCenter_x;
                mCenter_y = iCenter_y;
                mConf = iConf;
            }
            public float mWidth;     // rectangle width
            public float mAngle;//; = 0.0F;     // rectangle tilt angle [-45 45] in degrees
            public float mCenter_y;// = 0.0F;  // rectangle center y
            public float mCenter_x;// = 0.0F;  // rectangle center x
            public float mConf;// = 0.0F;
        };
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct TrackFaceInfo
        {
            [MarshalAs(UnmanagedType.Struct)]
            public FaceInfo box;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 144)]
            public int[] landmarks;// = new int[144];
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            public float[] headPose;// = new float[3];
            public float score;// = 0.0F;
            public UInt32 face_id;// = 0;
        }


        public static long ConvertDataTime2Long(DateTime dt)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan toNow = dt.Subtract(dtStart);
            var timeStamp = toNow.Ticks;
            timeStamp = long.Parse(timeStamp.ToString().Substring(0, timeStamp.ToString().Length - 4));
            return timeStamp;
        }
        #region dll
        // sdk初始化
        [DllImport("BaiduFaceApi.dll", EntryPoint = "sdk_init", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern int sdk_init(string model_path, bool id_card);
        // 是否授权
        [DllImport("BaiduFaceApi.dll", EntryPoint = "is_auth", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern bool is_auth();
        // 获取设备指纹
        [DllImport("BaiduFaceApi.dll", EntryPoint = "get_device_id", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr get_device_id();
        // sdk销毁
        [DllImport("BaiduFaceApi.dll", EntryPoint = "sdk_destroy", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern void sdk_destroy();

        #endregion
        #region 属性
        [DllImport("BaiduFaceApi.dll", EntryPoint = "face_attr", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr face_attr(string file_name);

        [DllImport("BaiduFaceApi.dll", EntryPoint = "face_attr_by_buf", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr face_attr_by_buf(byte[] buf, int size);
        #endregion
        #region 质量
        [DllImport("BaiduFaceApi.dll", EntryPoint = "face_quality", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr face_quality(string file_name);
        [DllImport("BaiduFaceApi.dll", EntryPoint = "face_quality_by_buf", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr face_quality_by_buf(byte[] buf, int size);
        #endregion
        #region 活体
        // 单目RGB静默活体检测（传入图片文件路径)
        [DllImport("BaiduFaceApi.dll", EntryPoint = "rgb_liveness_check", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr rgb_liveness_check(string file_name);

        // 单目RGB静默活体检测（传入图片文件二进制buffer)
        [DllImport("BaiduFaceApi.dll", EntryPoint = "rgb_liveness_check_by_buf", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr rgb_liveness_check_by_buf(byte[] buf, int size);
        #endregion
        #region 对比
        /// <summary>
        ///  提前加载库里所有数据到内存中
        /// </summary>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "load_db_face", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool load_db_face();
        /// <summary>
        ///  1:N人脸识别（传人脸图片文件和内存已加载的整个库数据比对）
        /// </summary>
        /// <param name="image"></param>
        /// <param name="user_top_num"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "identify_with_all", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr identify_with_all(string path, int user_top_num = 1);
        /// <summary>
        /// 1:N人脸识别（传人脸图片文件和内存已加载的整个库数据比对）
        /// </summary>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <param name="user_top_num"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "identify_by_buf_with_all", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr identify_by_buf_with_all(byte[] image, int size, int user_top_num = 1);
        #endregion

        #region 注册添加到数据库
        /// <summary>
        ///  人脸注册(传入图片文件路径)
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="group_id"></param>
        /// <param name="file_name"></param>
        /// <param name="user_info"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_add", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr user_add(string user_id, string group_id, string file_name,
            string user_info = "");
        /// <summary>
        ///  人脸注册(传入图片二进制buffer)
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="group_id"></param>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <param name="user_info"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_add_by_buf", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr user_add_by_buf(string user_id, string group_id, byte[] image,
            int size, string user_info = "");
        #endregion

        #region 删除
        /// <summary>
        ///  人脸删除
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="group_id"></param>
        /// <param name="face_token"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_face_delete", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr user_face_delete(string user_id, string group_id, string face_token);
        /// <summary>
        ///  用户删除
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="group_id"></param>
        /// <returns></returns>
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_delete", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr user_delete(string user_id, string group_id);


        // 人脸更新(传入图片文件路径)
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_update", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr user_update(string user_id, string group_id, string file_name,
            string user_info = "");
        // 人脸更新(传入图片二进制buffer)
        [DllImport("BaiduFaceApi.dll", EntryPoint = "user_update_by_buf", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr user_update_by_buf(string user_id, string group_id, byte[] image,
            int size, string user_info = "");


        // 查询用户信息
        [DllImport("BaiduFaceApi.dll", EntryPoint = "get_user_info", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr get_user_info(string user_id, string group_id);
        // 用户组列表查询
        [DllImport("BaiduFaceApi.dll", EntryPoint = "get_user_list", CharSet = CharSet.Ansi
            , CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr get_user_list(string group_id, int start = 0, int length = 100);
        #endregion
    }

}


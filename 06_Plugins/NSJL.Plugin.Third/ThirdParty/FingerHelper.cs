﻿using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DJ.ZEF.ThirdParty
{
    public class FingerHelper
    {
        private static readonly object locks = new object();
        private static FingerHelper manage;
        public static FingerHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new FingerHelper());
            }
        }

        IntPtr RawImgMap = Marshal.AllocHGlobal(360 * 256);  //宽高  图片
        //生成模板
        IntPtr AnsiFmrMap = Marshal.AllocHGlobal(1024);
        public int DeviceHandle = 0;     //设备句柄
        //图片  模板数组  索引   -1 表示匹配失败
        private Action<Bitmap, string, int> ActionCall = null;
        private List<string> ListUserList = null;
        public CommonResult Start(Action<Bitmap, string,int> action,List<string> tempList)
        {
            try
            {
                if (DeviceHandle == 0)
                {
                    DeviceHandle = DeviceHandler.FpStdLib_OpenDevice();
                    if (DeviceHandle == 0)
                    {
                        TextLogUtil.Info("指纹器初始化失败");
                        return new CommonResult() { result = false, message = "初始化失败" };
                    }
                }
                ActionCall = action;
                ListUserList = tempList;
                Task.Run(() => { MatchingFinger(); });
                return new CommonResult(){result = true};
            }
            catch (Exception e)
            {
                return new CommonResult(){result = false,message = e.Message};
            }
        }

        public void MatchingFinger()
        {
            try
            {
                while (DeviceHandle != 0)
                {
                    byte[] RawImage = new byte[360 * 256];
                    lock (locks)
                    {
                        if (DeviceHandle == 0)
                        {
                            return;
                        }

                        DeviceHandler.FpStdLib_GetImage(DeviceHandle, RawImgMap);
                        Marshal.Copy(RawImgMap, RawImage, 0, 360 * 256);

                        //图片质量  >  50
                        int qr = DeviceHandler.FpStdLib_GetImageQuality(DeviceHandle, RawImgMap);
                        if (qr <= 50)
                        {
                            continue;
                        }
                    }

                    int size = DeviceHandler.FpStdLib_CreateANSITemplate(DeviceHandle, RawImgMap, AnsiFmrMap);
                    if (size >= 0)
                    {
                        byte[] tmpFMR = new byte[size];
                        Marshal.Copy(AnsiFmrMap, tmpFMR, 0, size);

                        var base64 = Convert.ToBase64String(tmpFMR);

                        if (ListUserList != null && ListUserList.Count > 0)
                        {
                            #region 1:1
                            var index = -1;
                            for (int i = 0; i < ListUserList.Count; i++)
                            {
                                var bytes = Convert.FromBase64String(ListUserList[i]);
                                var fen = DeviceHandler.FpStdLib_CompareTemplates(DeviceHandle, AnsiFmrMap, bytes);
                                if (fen > 30)
                                {
                                    index = i;
                                    break;
                                }
                            }
                            if (ActionCall != null)
                            {
                                Task.Run(() =>
                                {
                                    ActionCall(GetImageWithByte(RawImage), base64, index);
                                });
                            }
                            #endregion
                            #region 1:N 匹配  有问题用不了
                            //byte[] fpdb = new byte[tempList.Count * 512];
                            //for (int i = 0; i < tempList.Count; i++)
                            //{
                            //    var bytes = Convert.FromBase64String(tempList[i]);
                            //    Array.Copy(bytes, 0, fpdb, i * 512, bytes.Length);
                            //}

                            //var index = DeviceHandler.FpStdLib_SearchingANSITemplates(DeviceHandle, AnsiFmrMap, tempList.Count, fpdb, 40);
                            //action(RawImage, base64, index);
                            #endregion
                            break;
                        }
                        else
                        {
                            if (ActionCall != null)
                            {
                                Task.Run(() =>
                                {
                                    ActionCall(GetImageWithByte(RawImage), base64, -1);
                                });
                            }
                            break;
                        }
                    }
                    break;
                }
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }



        public int CompareTemplates(List<string> tempList,string base64)
        {
            if (DeviceHandle == 0)
            {
                DeviceHandle = DeviceHandler.FpStdLib_OpenDevice();
            }
            var tempbytes = Convert.FromBase64String(base64);
            var index = -1;
            for (int i = 0; i < tempList.Count; i++)
            {
                var bytes = Convert.FromBase64String(tempList[i]);
                var fen = DeviceHandler.FpStdLib_CompareTemplates(DeviceHandle, tempbytes, bytes);
                if (fen > 30)
                {
                    index = i;
                    break;
                }
            }

            Stop();
            return index;
        }
        public void Stop()
        {
            lock (locks)
            {
                ActionCall = null;
                ListUserList = null;
                if (DeviceHandle == 0)
                {
                    return;
                }
                try
                {
                    DeviceHandler.FpStdLib_CloseDevice(DeviceHandle);
                    //Marshal.FreeHGlobal(RawImgMap);
                    //Marshal.FreeHGlobal(AnsiFmrMap);
                    DeviceHandle = 0;
                }
                catch (Exception e)
                {
                    TextLogUtil.Info(e.Message);
                }
            }
        }

        public Bitmap GetImageWithByte(byte[] rawData)
        {
            Bitmap bsss = new Bitmap(256, 360, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            ColorPalette pal = bsss.Palette;
            for (int i = 0; i < pal.Entries.Length; i++)
            {
                pal.Entries[i] = System.Drawing.Color.FromArgb(255, i, i, i);
            }
            BitmapData data = bsss.LockBits(new System.Drawing.Rectangle(0, 0, 256, 360), ImageLockMode.ReadWrite,
                System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            Marshal.Copy(rawData, 0, data.Scan0, 256 * 360);
            bsss.UnlockBits(data);
            bsss.Palette = pal;
            bsss.SetResolution(500, 500);
            return bsss;
        }
    }
}

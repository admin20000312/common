﻿using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using DJ.ZEF.ThirdParty;
using Quartz.Util;
using System.Data.Entity.Migrations;
using System.Data.Entity.Core.Metadata.Edm;

namespace NSJL.Biz.Background.Client
{
    public class CommBiz
    {
        public string CabinetName = null;
        public CommBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            CabinetName = ini.readKey("BoxConfig", "cabinetName");
        }
        public SJLDbContext db=new SJLDbContext();
        #region
        public CommonResult<AdminInfo> Login(string account, string password)
        {

            var info = db.AdminInfo.Where(p => p.Account == account).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<AdminInfo>() { result = false, message = "用户不存在" };
            }
            if (info.Password != password)
            {
                return new CommonResult<AdminInfo>() { result = false, message = "密码不正确" };
            }
            return new CommonResult<AdminInfo>() { result = true, data = info };
        }
        public CommonResult GetFreeBox()
        {
            var info = db.BoxInfo.Where(p => p.IsFree == true).ToList();
            if (info.Count <= 0)
            {
                return new CommonResult() { result = false, message = "没有可用箱门" };
            }
            return new CommonResult() { result = true, message = info.Count.ToString() };
        }
        public CommonResult<List<int>> GetNoBindBoxNum(string userId = null)
        {
            var infos = db.BoxInfo.Select(p=>p.BoxNum).ToList();
            if (infos.Count <= 0)
            {
                return new CommonResult<List<int>>() { result = false, message = "没有空闲箱门" };
            }
            var boxs = infos;
            List<string> lists = new List<string>();
            if (userId != null)
            {
                lists = db.UserInfo.Where(p => p.Id != userId).Select(p => p.BoxNums).ToList();
            }
            else
            {
                lists = db.UserInfo.Select(p => p.BoxNums).ToList();
            }
            foreach(var list in lists)
            {
                if (infos.Contains(Convert.ToInt32(list)))
                {
                    boxs.Remove(Convert.ToInt32(list));
                }
            }
            boxs.Sort();
            return new CommonResult<List<int>>() { result = true, data = boxs };
        }
        
        
        public CommonResult<UserInfo> GetUserInfoWithFaceId(string faceId)
        {
            var info = db.UserInfo.Where(p => p.FaceId == faceId).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult<UserInfo>() { result = false, message = "用户不存在" };
            }
            return new CommonResult<UserInfo>() { result = true, data = info };
        }
        
        public CommonResult<List<BoxInfo>> GetBoxListWithUserId()
        {
            var boxs = db.BoxInfo.ToList();
            return new CommonResult<List<BoxInfo>>() { result = true, data = boxs };
        }
        public CommonResult<UserInfo> GetUserByBindBoxNum(string boxNum)
        {
            var user = db.UserInfo.Where(p=>p.BoxNums == boxNum).FirstOrDefault();
            return new CommonResult<UserInfo>() { result = true, data = user };
        }
        
        public List<UserInfo> GetAllUser()
        {
            var users = db.UserInfo.ToList();
            return  users;
        }
        public List<UserInfo> GetAllUserHaveFace()
        {
            var users = db.UserInfo.Where(p=>p.HeadPic != null).ToList();
            return users;
        }

        public CommonResult<List<VisitorLogInfo>> GetAllVisitorLogInfo()
        {
            var info = db.VisitorLogInfo.ToList();
            if (info == null)
            {
                return new CommonResult<List<VisitorLogInfo>>() { result = false, message = "游客记录为空" };
            }
            return new CommonResult<List<VisitorLogInfo>>() { result = true, data = info };
        }
        public CommonResult ClearVisitorLogInfo()
        {
            try
            {
                var info = db.VisitorLogInfo.ToList();
                db.VisitorLogInfo.RemoveRange(info);
                db.SaveChanges();
                return new CommonResult() { result = true };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false,message = ex.Message };
            }
        }
        
        public CommonResult<List<UserLogInfo>> GetAllUserLogInfo()
        {
            var info = db.UserLogInfo.ToList();
            if (info == null)
            {
                return new CommonResult<List<UserLogInfo>>() { result = false, message = "用户记录为空" };
            }
            return new CommonResult<List<UserLogInfo>>() { result = true, data = info };
        }
        public CommonResult ClearUserLogInfo()
        {
            try
            {
                var info = db.UserLogInfo.ToList();
                db.UserLogInfo.RemoveRange(info);
                db.SaveChanges();
                return new CommonResult() { result = true };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false,message = ex.Message };
            } 
        }


        public CommonResult<List<AdminLogInfo>> GetAllAdminLogInfo()
        {
            var info = db.AdminLogInfo.ToList();
            if (info == null)
            {
                return new CommonResult<List<AdminLogInfo>>() { result = false, message = "用户记录为空" };
            }
            return new CommonResult<List<AdminLogInfo>>() { result = true, data = info };
        }
        public CommonResult<List<AdminInfo>> GetAllAdminInfo()
        {
            var info = db.AdminInfo.Where(p=>p.Permission != "超级管理员").ToList();
            if (info == null)
            {
                return new CommonResult<List<AdminInfo>>() { result = false, message = "管理员为空" };
            }
            return new CommonResult<List<AdminInfo>>() { result = true, data = info };
        }

        //添加或者编辑管理员
        public CommonResult EditAdminInfo(AdminInfo info)
        {
            try
            {
                List<AdminInfo> list = db.AdminInfo.ToList();

                if (string.IsNullOrWhiteSpace(info.Id)) //新增
                {
                    if (db.UserInfo.Any(p => p.UserName == info.UserName))
                    {
                        return new CommonResult() { result = false, message = "管理员名称已存在，无法添加" };
                    }
                    if (db.UserInfo.Any(p => p.Account == info.Account))
                    {
                        return new CommonResult() { result = false, message = "账号已存在，无法添加" };
                    }
                    if (!string.IsNullOrWhiteSpace(info.FaceId))
                    {
                        if (db.UserInfo.Any(p => p.FaceId == info.FaceId))
                        {
                            return new CommonResult() { result = false, message = "人脸已存在，无法添加" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.Finger))
                    {
                        var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Finger).ToList(), info.Finger);
                        if (ints >= 0)
                        {
                            return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.CardNum))
                    {
                        if (db.UserInfo.Any(p => p.CardNum == info.CardNum))
                        {
                            return new CommonResult() { result = false, message = "卡号已存在，无法添加" };
                        }
                    }
                    info.Id = Guid.NewGuid().ToString("N");
                    info.CreateTime = DateTime.Now;
                    info.Permission = "普通管理员";
                    db.AdminInfo.Add(info);
                }
                else //修改
                {
                    if (db.UserInfo.Any(p => p.UserName == info.UserName && p.Id != info.Id))
                    {
                        return new CommonResult() { result = false, message = "管理员名称已存在，无法添加" };
                    }
                    if (db.UserInfo.Any(p => p.Account == info.Account && p.Id != info.Id))
                    {
                        return new CommonResult() { result = false, message = "账号已存在，无法修改" };
                    }
                    if (!string.IsNullOrWhiteSpace(info.FaceId))
                    {
                        if (db.UserInfo.Any(p => p.FaceId == info.FaceId && p.Id != info.Id))
                        {
                            return new CommonResult() { result = false, message = "人脸已存在，无法修改" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.Finger))
                    {
                        var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Finger).ToList(), info.Finger);
                        if (ints >= 0)
                        {
                            if (list[ints].Id != info.Id)
                            {
                                return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.CardNum))
                    {
                        if (db.UserInfo.Any(p => p.CardNum == info.CardNum && p.Id != info.Id))
                        {
                            return new CommonResult() { result = false, message = "卡号已存在，无法修改" };
                        }
                    }
                    AdminInfo adminInfo = db.AdminInfo.Where(p => p.Id == info.Id).FirstOrDefault();
                    if (adminInfo == null)
                    {
                        return new CommonResult() { result = false, message = "修改失败,该管理员不存在" };
                    }
                    string permission = adminInfo.Permission;
                    adminInfo = info;
                    adminInfo.Permission = permission;
                    db.AdminInfo.AddOrUpdate(adminInfo);
                }
                db.SaveChanges();
                return new CommonResult() { result = true, message = "操作成功" };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
        //删除管理员
        public CommonResult DeleteAdminInfo(string id)
        {
            try
            {
                AdminInfo admininfo = db.AdminInfo.Where((p) => p.Id == id).FirstOrDefault();
                db.AdminInfo.Remove(admininfo);
                db.SaveChanges();
                return new CommonResult() { result = true, message = "操作成功" };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
        public CommonResult<List<UserInfo>> GetAllUserInfo()
        {
            try
            {
                var info = db.UserInfo.Where(p=>p.IsVisitor == false).ToList();
                if (info == null)
                {
                    return new CommonResult<List<UserInfo>>() { result = false, message = "用户为空" };
                }
                return new CommonResult<List<UserInfo>>() { result = true, data = info };
            }catch (Exception ex)
            {
                return new CommonResult<List<UserInfo>>() { result = false,message = ex.Message };
            }
        }
        //添加或者编辑用户
        public CommonResult EditUserInfo(UserInfo info)
        {
            try
            {
                List<UserInfo> list = db.UserInfo.ToList();
                if (string.IsNullOrWhiteSpace(info.Id)) //新增
                {
                    if (db.UserInfo.Any(p => p.Account == info.Account))
                    {
                        return new CommonResult() { result = false, message = "账号已存在，无法添加" };
                    }
                    if (!string.IsNullOrWhiteSpace(info.FaceId))
                    {
                        if (db.UserInfo.Any(p => p.FaceId == info.FaceId))
                        {
                            return new CommonResult() { result = false, message = "人脸已存在，无法添加" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.Finger))
                    {
                        var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Finger).ToList(), info.Finger);
                        if (ints >= 0)
                        {
                            return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.CardNum))
                    {
                        if (db.UserInfo.Any(p => p.CardNum == info.CardNum))
                        {
                            return new CommonResult() { result = false, message = "卡号已存在，无法添加" };
                        }
                    }
                    info.Id = Guid.NewGuid().ToString("N");
                    info.CreateTime = DateTime.Now;
                    info.IsVisitor = false;
                    db.UserInfo.Add(info);
                }
                else //修改
                {
                    if (db.UserInfo.Any(p => p.Account == info.Account && p.Id != info.Id))
                    {
                        return new CommonResult() { result = false, message = "账号已存在，无法修改" };
                    }
                    if (!string.IsNullOrWhiteSpace(info.FaceId))
                    {
                        if (db.UserInfo.Any(p => p.FaceId == info.FaceId && p.Id != info.Id))
                        {
                            return new CommonResult() { result = false, message = "人脸已存在，无法修改" };
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.Finger))
                    {
                        var ints = FingerHelper.GetInstance().CompareTemplates(list.Select(p => p.Finger).ToList(), info.Finger);
                        if (ints >= 0)
                        {
                            if (list[ints].Id != info.Id)
                            {
                                return new CommonResult() { result = false, message = "指纹已存在，无法添加" };
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(info.CardNum))
                    {
                        if (db.UserInfo.Any(p => p.CardNum == info.CardNum && p.Id != info.Id))
                        {
                            return new CommonResult() { result = false, message = "卡号已存在，无法修改" };
                        }
                    }

                    UserInfo userInfo = db.UserInfo.Where(p => p.Id == info.Id).FirstOrDefault();
                    if (userInfo == null)
                    {
                        return new CommonResult() { result = false, message = "修改失败,该用户不存在" };
                    }
                    bool? Isvisitor = userInfo.IsVisitor;
                    var time = userInfo.CreateTime;
                    userInfo = info;
                    userInfo.IsVisitor = Isvisitor;
                    userInfo.CreateTime = time;
                    db.UserInfo.AddOrUpdate(userInfo);
                }
                db.SaveChanges();
                return new CommonResult() { result = true, message = "操作成功" };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
        //删除用户
        public CommonResult DeleteUserInfo(string id)
        {
            try
            {
                UserInfo userInfo = db.UserInfo.Where((p) => p.Id == id).FirstOrDefault();
                db.UserInfo.Remove(userInfo);
                db.SaveChanges();
                return new CommonResult() { result = true, message = "操作成功" };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }
        //批量删除用户
        public CommonResult DeleteUserInfoList(List<string> list)
        {
            try
            {
                foreach (var item in list)
                {
                    UserInfo userInfo = db.UserInfo.Where((p) => p.Id == item).FirstOrDefault();
                    userInfo.HeadPic = null;
                    userInfo.FaceId = null;
                    db.UserInfo.AddOrUpdate(userInfo);
                }
                db.SaveChanges();
                return new CommonResult() { result = true, message = "操作成功" };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }

        //判断数据库中用户表是否存在这个面部id
        public bool IsFaceInUserInfo(string faceId)
        {
            return db.UserInfo.Any(p => p.FaceId == faceId);
        }

        //判断数据库中管理员表是否存在这个面部id
        public bool IsFaceInAdminInfo(string faceId)
        {
            return db.AdminInfo.Any(p => p.FaceId == faceId);
        }

        public List<ModelInfo> GetModelInfo() 
        {
            var models = db.ModelInfo.ToList();
            return models;
        } 
        public CommonResult EditModelInfoByName(string name,bool b) 
        {
            try
            {
                var model = db.ModelInfo.Where(p => p.ModelName == name).FirstOrDefault();
                model.IsChoose = b;
                db.ModelInfo.AddOrUpdate(model);
                db.SaveChanges();
                return new CommonResult() { result = true };
            }catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                return new CommonResult() { result = false};
            }
            
        }
        public CommonResult EditAdminPassword(string username, string old, string news)
        {
            var info = db.AdminInfo.Where(p => p.UserName == username).FirstOrDefault();
            if (info.Password != old)
            {
                return new CommonResult() { result = false, message = "老密码错误" };
            }

            info.Password = news;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }

        
        #endregion

    }
}

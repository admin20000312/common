﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetUserInfo : INotifyPropertyChanged
    {
        private string _Name = string.Empty;
        private string _Code = string.Empty;
        private string _Mobile = string.Empty;
        private string _BoxNums = string.Empty;
        private DateTime? _CreateTime = null;
        private string _Password = string.Empty;
        private string _Type = string.Empty;
        private string _Features = string.Empty;
        private string _GoodsClasss = string.Empty;
        private string _Headpic = string.Empty;
        
        public string Id { get; set; }
        //姓名
        public string Name { get{return _Name;} set { UpdateProperty(ref _Name, value); } }
        public string Code { get { return _Code; } set { UpdateProperty(ref _Code, value); } }
        public string Mobile { get { return _Mobile; } set { UpdateProperty(ref _Mobile, value); } }
        public string BoxNums { get { return _BoxNums; } set { UpdateProperty(ref _BoxNums, value); } }
        //创建时间
        public DateTime? CreateTime { get { return _CreateTime; } set { UpdateProperty(ref _CreateTime, value); } }
        public string Password { get { return _Password; } set { UpdateProperty(ref _Password, value); } }
        public string Type { get { return _Type; } set { UpdateProperty(ref _Type, value); } }
        public string Features { get { return _Features; } set { UpdateProperty(ref _Features, value); } }
        public string GoodsClasss { get { return _GoodsClasss; } set { UpdateProperty(ref _GoodsClasss, value); } }
        public string Headpic { get { return _Headpic; } set { UpdateProperty(ref _Headpic, value); } }

        private void UpdateProperty<T>(ref T properValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(properValue, newValue))
            {
                return;
            }
            properValue = newValue;

            OnPropertyChanged(propertyName);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }



    public class GetGoodsList : INotifyPropertyChanged
    {
        private string _GoodsId = string.Empty;
        private string _GoodsName = string.Empty;
        private string _GoodsNum = string.Empty;
        private string _GoodsClasss = string.Empty;
        private int? _StockNum ;
        private int? _StandardNum ;
        public string Id { get; set; }
        public string CabName { get; set; }
        public string BoxNum { get; set; }
        public string GoodsId { get { return _GoodsId; } set { UpdateProperty(ref _GoodsId, value); } }
        public string GoodsNum { get { return _GoodsNum; } set { UpdateProperty(ref _GoodsNum, value); } }
        public string GoodsClasss { get { return _GoodsClasss; } set { UpdateProperty(ref _GoodsClasss, value); } }
        public string GoodsName { get { return _GoodsName; } set { UpdateProperty(ref _GoodsName, value); } }
        public int? StockNum { get { return _StockNum; } set { UpdateProperty(ref _StockNum, value); } }
        public int? StandardNum { get { return _StandardNum; } set { UpdateProperty(ref _StandardNum, value); } }



        private void UpdateProperty<T>(ref T properValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(properValue, newValue))
            {
                return;
            }
            properValue = newValue;

            OnPropertyChanged(propertyName);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }




    public class GetGoodsClassList
    {
        private string _Name = string.Empty;
        private DateTime? _CreateTime = null;
        public string Id { get; set; }
        public string Name { get { return _Name; } set { UpdateProperty(ref _Name, value); } }
        public DateTime? CreateTime { get { return _CreateTime; } set { UpdateProperty(ref _CreateTime, value); } }
        private void UpdateProperty<T>(ref T properValue, T newValue, [CallerMemberName] string propertyName = "")
        {
            if (object.Equals(properValue, newValue))
            {
                return;
            }
            properValue = newValue;

            OnPropertyChanged(propertyName);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }


}

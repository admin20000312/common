﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NSJL.DomainModel.Background.Client
{

    [XmlRoot("OT_DATA")]
    public class OT_DATA : List<item>
    {
        //[XmlArray("items")]
        //public List<item> items { get; set; }
    }
    //[Serializable, XmlType("item")]
    public class item
    {
        //----------补库
        public string RSNUM { get; set; }
        public string RSPOS { get; set; }
        public string WERKS { get; set; }
        public string MATNR { get; set; }
        public string CHARG { get; set; }
        public string LGORT { get; set; }
        public string UMLGO { get; set; }
        public string BWART { get; set; }
        public string MENGE { get; set; }
        public string MEINS { get; set; }
        public string BUDAT_MKPF { get; set; }
        //----------------

        //--------物料类别
        //public string MATNR { get; set; }
        public string MATKL { get; set; }
        public string WGBEZ { get; set; }
        //----------------

        //-----------成本中心
        public string KOKRS { get; set; }
        public string KOSTL { get; set; }
        public string KTEXT { get; set; }
        public string DATAB { get; set; }
        public string DATBI { get; set; }
        //----------------

    }

        

    
}
